import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { RealAlgebra } from '../../src/index.mjs'
let ralg = new RealAlgebra()

let a = ralg.num(1, 2)
let b = ralg.num(1, 4)

// GOOD-PRACTICE!
a = ralg.isub(a, b)
testDriver.test(() => { return typeof a === 'number' }, true, 'src/real-algebra.mjs~RealAlgebra#isub-example1_0', false)
testDriver.test(() => { return ralg.eq(a, ralg.num(1, 4)) }, true, 'src/real-algebra.mjs~RealAlgebra#isub-example1_1', false)
