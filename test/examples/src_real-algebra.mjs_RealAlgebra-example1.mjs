import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { RealAlgebra } from '../../src/index.mjs'
let ralg = new RealAlgebra()

let a = ralg.num(1, 2)
let b = ralg.num(1, 3)
testDriver.test(() => { return ralg.eq(a, b) }, false, 'src/real-algebra.mjs~RealAlgebra-example1_0', false)
testDriver.test(() => { return ralg.isZero(ralg.num(1, 999999)) }, false, 'src/real-algebra.mjs~RealAlgebra-example1_1', false)
