import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { Monomial as M, Polynomial as P } from '../../src/index.mjs'

let a = new P(new M(-1), new M(1))

// toJSON method is called by JSON.stringify
let s = JSON.stringify(a)

testDriver.test(() => { return typeof s }, 'string', 'src/number-field.mjs~Polynomial#toJSON-example0_0', false)
