import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { Monomial as M, Polynomial as P } from '../../src/index.mjs'

let a = new P(new M(-1, 2, 3), new M(1, 2, 5))
let b = a
let c = new P(new M(-1, 1, 3), new M(1, 1, 5))
a.iadd(a)

testDriver.test(() => { return a === b }, true, 'src/number-field.mjs~Polynomial#iadd-example1_0', false)
testDriver.test(() => { return a.equals(c) }, true, 'src/number-field.mjs~Polynomial#iadd-example1_1', false)
testDriver.test(() => { return b.equals(c) }, true, 'src/number-field.mjs~Polynomial#iadd-example1_2', false)

testDriver.test(() => { return a.iadd(null) }, Error, 'src/number-field.mjs~Polynomial#iadd-example1_3', false)
