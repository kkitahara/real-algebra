import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { RealAlgebra } from '../../src/index.mjs'
let ralg = new RealAlgebra()

let a = ralg.num(1, 2)

let b = ralg.cjg(a)
testDriver.test(() => { return typeof b === 'number' }, true, 'src/real-algebra.mjs~RealAlgebra#cjg-example0_0', false)
testDriver.test(() => { return b !== a }, false, 'src/real-algebra.mjs~RealAlgebra#cjg-example0_1', false)
testDriver.test(() => { return ralg.eq(a, ralg.num(1, 2)) }, true, 'src/real-algebra.mjs~RealAlgebra#cjg-example0_2', false)
testDriver.test(() => { return ralg.eq(b, ralg.num(1, 2)) }, true, 'src/real-algebra.mjs~RealAlgebra#cjg-example0_3', false)
