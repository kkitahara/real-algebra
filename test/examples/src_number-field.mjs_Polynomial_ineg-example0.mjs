import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { Monomial as M, Polynomial as P } from '../../src/index.mjs'

let a = new P(new M(1, 2), new M(1, 2, 5))
let b = a
let c = new P(new M(-1, 2), new M(-1, 2, 5))
let d = a.copy()
a.ineg()

testDriver.test(() => { return a === b }, true, 'src/number-field.mjs~Polynomial#ineg-example0_0', false)
testDriver.test(() => { return a === d }, false, 'src/number-field.mjs~Polynomial#ineg-example0_1', false)
testDriver.test(() => { return a.equals(c) }, true, 'src/number-field.mjs~Polynomial#ineg-example0_2', false)
testDriver.test(() => { return a.equals(d) }, false, 'src/number-field.mjs~Polynomial#ineg-example0_3', false)
testDriver.test(() => { return b.equals(c) }, true, 'src/number-field.mjs~Polynomial#ineg-example0_4', false)
testDriver.test(() => { return b.equals(d) }, false, 'src/number-field.mjs~Polynomial#ineg-example0_5', false)
