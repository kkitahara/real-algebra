import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { ExactRealAlgebra as RealAlgebra, Polynomial as P }
  from '../../src/index.mjs'
let ralg = new RealAlgebra()

let a = ralg.num(1, 2)
let b = ralg.num(1, 4)
let c = a

// GOOD-PRACTICE!
a = ralg.idiv(a, b)
testDriver.test(() => { return a === c }, true, 'src/real-algebra.mjs~RealAlgebra#idiv-example2_0', false)
testDriver.test(() => { return typeof a === 'number' }, false, 'src/real-algebra.mjs~RealAlgebra#idiv-example2_1', false)
testDriver.test(() => { return a instanceof P }, true, 'src/real-algebra.mjs~RealAlgebra#idiv-example2_2', false)
testDriver.test(() => { return ralg.eq(a, ralg.num(2)) }, true, 'src/real-algebra.mjs~RealAlgebra#idiv-example2_3', false)
testDriver.test(() => { return ralg.eq(c, ralg.num(2)) }, true, 'src/real-algebra.mjs~RealAlgebra#idiv-example2_4', false)
