import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import bigRat from 'big-rational'
import { Monomial as M } from '../../src/index.mjs'

let a = new M(1, 2, 3)
let b = a.isubFromCoef(bigRat(1, 3))

testDriver.test(() => { return b === a }, true, 'src/number-field.mjs~Monomial#isubFromCoef-example0_0', false)
testDriver.test(() => { return a.equals(new M(1, 6, 3)) }, true, 'src/number-field.mjs~Monomial#isubFromCoef-example0_1', false)
testDriver.test(() => { return b.equals(new M(1, 6, 3)) }, true, 'src/number-field.mjs~Monomial#isubFromCoef-example0_2', false)
