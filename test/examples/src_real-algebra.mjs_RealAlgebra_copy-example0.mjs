import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { RealAlgebra, ExactRealAlgebra } from '../../src/index.mjs'
let ralg = new RealAlgebra()
let eralg = new ExactRealAlgebra()

// copy does not work for numerical algebra
let a = ralg.num(1, 2)
testDriver.test(() => { return ralg.copy(a) === a }, true, 'src/real-algebra.mjs~RealAlgebra#copy-example0_0', false)

let b = eralg.num(1, 2)
let c = eralg.copy(b)
testDriver.test(() => { return b === c }, false, 'src/real-algebra.mjs~RealAlgebra#copy-example0_1', false)
testDriver.test(() => { return b.equals(c) }, true, 'src/real-algebra.mjs~RealAlgebra#copy-example0_2', false)
