import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { ExactRealAlgebra as RealAlgebra, Polynomial as P }
  from '../../src/index.mjs'
let ralg = new RealAlgebra()

let a = ralg.num(1, 2)

let b = ralg.cjg(a)
testDriver.test(() => { return typeof b === 'number' }, false, 'src/real-algebra.mjs~RealAlgebra#cjg-example1_0', false)
testDriver.test(() => { return b instanceof P }, true, 'src/real-algebra.mjs~RealAlgebra#cjg-example1_1', false)
testDriver.test(() => { return b !== a }, true, 'src/real-algebra.mjs~RealAlgebra#cjg-example1_2', false)
testDriver.test(() => { return ralg.eq(a, ralg.num(1, 2)) }, true, 'src/real-algebra.mjs~RealAlgebra#cjg-example1_3', false)
testDriver.test(() => { return ralg.eq(b, ralg.num(1, 2)) }, true, 'src/real-algebra.mjs~RealAlgebra#cjg-example1_4', false)
