import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { ExactRealAlgebra as RealAlgebra } from '../../src/index.mjs'
let ralg = new RealAlgebra()
let a, b, c

// generate a new number
a = ralg.num(1, 2, 5)
testDriver.test(() => { return a.toString() }, '(1 / 2)sqrt(5)', 'src/real-algebra.mjs~RealAlgebra-example0_0', false)

a = ralg.num(1, 2)
testDriver.test(() => { return a.toString() }, '1 / 2', 'src/real-algebra.mjs~RealAlgebra-example0_1', false)

a = ralg.num(3)
testDriver.test(() => { return a.toString() }, '3', 'src/real-algebra.mjs~RealAlgebra-example0_2', false)

// generate a new number (short form, since v1.2.0)
a = ralg.$(1, 2, 5)
testDriver.test(() => { return a.toString() }, '(1 / 2)sqrt(5)', 'src/real-algebra.mjs~RealAlgebra-example0_3', false)

a = ralg.$(1, 2)
testDriver.test(() => { return a.toString() }, '1 / 2', 'src/real-algebra.mjs~RealAlgebra-example0_4', false)

a = ralg.$(3)
testDriver.test(() => { return a.toString() }, '3', 'src/real-algebra.mjs~RealAlgebra-example0_5', false)

// copy (create a new object)
a = ralg.num(1, 2, 5)
b = ralg.copy(a)
testDriver.test(() => { return b.toString() }, '(1 / 2)sqrt(5)', 'src/real-algebra.mjs~RealAlgebra-example0_6', false)

// equality
a = ralg.num(1, 2, 5)
b = ralg.num(3, 2, 5)
testDriver.test(() => { return ralg.eq(a, b) }, false, 'src/real-algebra.mjs~RealAlgebra-example0_7', false)

b = ralg.num(1, 2, 5)
testDriver.test(() => { return ralg.eq(a, b) }, true, 'src/real-algebra.mjs~RealAlgebra-example0_8', false)

// inequality
a = ralg.num(1, 2, 5)
b = ralg.num(3, 2, 5)
testDriver.test(() => { return ralg.ne(a, b) }, true, 'src/real-algebra.mjs~RealAlgebra-example0_9', false)

b = ralg.num(1, 2, 5)
testDriver.test(() => { return ralg.ne(a, b) }, false, 'src/real-algebra.mjs~RealAlgebra-example0_10', false)

// isZero
testDriver.test(() => { return ralg.isZero(ralg.num(0)) }, true, 'src/real-algebra.mjs~RealAlgebra-example0_11', false)
testDriver.test(() => { return ralg.isZero(ralg.num(1, 2, 5)) }, false, 'src/real-algebra.mjs~RealAlgebra-example0_12', false)
testDriver.test(() => { return ralg.isZero(ralg.num(-1, 2, 5)) }, false, 'src/real-algebra.mjs~RealAlgebra-example0_13', false)

// isPositive
testDriver.test(() => { return ralg.isPositive(ralg.num(0)) }, false, 'src/real-algebra.mjs~RealAlgebra-example0_14', false)
testDriver.test(() => { return ralg.isPositive(ralg.num(1, 2, 5)) }, true, 'src/real-algebra.mjs~RealAlgebra-example0_15', false)
testDriver.test(() => { return ralg.isPositive(ralg.num(-1, 2, 5)) }, false, 'src/real-algebra.mjs~RealAlgebra-example0_16', false)

// isNegative
testDriver.test(() => { return ralg.isNegative(ralg.num(0)) }, false, 'src/real-algebra.mjs~RealAlgebra-example0_17', false)
testDriver.test(() => { return ralg.isNegative(ralg.num(1, 2, 5)) }, false, 'src/real-algebra.mjs~RealAlgebra-example0_18', false)
testDriver.test(() => { return ralg.isNegative(ralg.num(-1, 2, 5)) }, true, 'src/real-algebra.mjs~RealAlgebra-example0_19', false)

// isInteger (since v1.1.0)
testDriver.test(() => { return ralg.isInteger(ralg.num(0)) }, true, 'src/real-algebra.mjs~RealAlgebra-example0_20', false)
testDriver.test(() => { return ralg.isInteger(ralg.num(6, 3)) }, true, 'src/real-algebra.mjs~RealAlgebra-example0_21', false)
testDriver.test(() => { return ralg.isInteger(ralg.num(1, 2)) }, false, 'src/real-algebra.mjs~RealAlgebra-example0_22', false)
testDriver.test(() => { return ralg.isInteger(ralg.num(2, 1, 3)) }, false, 'src/real-algebra.mjs~RealAlgebra-example0_23', false)

// addition
a = ralg.num(1, 2, 5)
b = ralg.num(1, 2, 1)
// new object is generated
c = ralg.add(a, b)
testDriver.test(() => { return c.toString() }, '1 / 2 + (1 / 2)sqrt(5)', 'src/real-algebra.mjs~RealAlgebra-example0_24', false)

// in-place addition
a = ralg.num(1, 2, 5)
b = ralg.num(1, 2, 1)
// new object is not generated
a = ralg.iadd(a, b)
testDriver.test(() => { return a.toString() }, '1 / 2 + (1 / 2)sqrt(5)', 'src/real-algebra.mjs~RealAlgebra-example0_25', false)

// subtraction
a = ralg.num(1, 2, 5)
b = ralg.num(1, 2, 1)
// new object is generated
c = ralg.sub(a, b)
testDriver.test(() => { return c.toString() }, '-1 / 2 + (1 / 2)sqrt(5)', 'src/real-algebra.mjs~RealAlgebra-example0_26', false)

// in-place subtraction
a = ralg.num(1, 2, 5)
b = ralg.num(1, 2, 1)
// new object is not generated
a = ralg.isub(a, b)
testDriver.test(() => { return a.toString() }, '-1 / 2 + (1 / 2)sqrt(5)', 'src/real-algebra.mjs~RealAlgebra-example0_27', false)

// multiplication
a = ralg.iadd(ralg.num(1, 2), ralg.num(1, 2, 5))
b = ralg.iadd(ralg.num(-1, 2), ralg.num(1, 2, 5))
// new object is generated
c = ralg.mul(a, b)
testDriver.test(() => { return c.toString() }, '1', 'src/real-algebra.mjs~RealAlgebra-example0_28', false)

// in-place multiplication
a = ralg.iadd(ralg.num(1, 2), ralg.num(1, 2, 5))
b = ralg.iadd(ralg.num(-1, 2), ralg.num(1, 2, 5))
// new object is not generated
a = ralg.imul(a, b)
testDriver.test(() => { return a.toString() }, '1', 'src/real-algebra.mjs~RealAlgebra-example0_29', false)

// division
a = ralg.iadd(ralg.num(1, 2), ralg.num(1, 2, 5))
b = ralg.iadd(ralg.num(-1, 2), ralg.num(1, 2, 5))
// new object is generated
c = ralg.div(a, b)
testDriver.test(() => { return c.toString() }, '3 / 2 + (1 / 2)sqrt(5)', 'src/real-algebra.mjs~RealAlgebra-example0_30', false)

// in-place division
a = ralg.iadd(ralg.num(1, 2), ralg.num(1, 2, 5))
b = ralg.iadd(ralg.num(-1, 2), ralg.num(1, 2, 5))
// new object is not generated
a = ralg.idiv(a, b)
testDriver.test(() => { return a.toString() }, '3 / 2 + (1 / 2)sqrt(5)', 'src/real-algebra.mjs~RealAlgebra-example0_31', false)

// multiplication by -1
a = ralg.iadd(ralg.num(1, 2), ralg.num(1, 2, 5))
// new object is generated
b = ralg.neg(a)
testDriver.test(() => { return b.toString() }, '-1 / 2 - (1 / 2)sqrt(5)', 'src/real-algebra.mjs~RealAlgebra-example0_32', false)

// in-place multiplication by -1
a = ralg.iadd(ralg.num(1, 2), ralg.num(1, 2, 5))
// new object is not generated
a = ralg.ineg(a)
testDriver.test(() => { return a.toString() }, '-1 / 2 - (1 / 2)sqrt(5)', 'src/real-algebra.mjs~RealAlgebra-example0_33', false)

// absolute value
a = ralg.iadd(ralg.num(1, 2), ralg.num(-1, 2, 5))
// new object is generated
b = ralg.abs(a)
testDriver.test(() => { return b.toString() }, '-1 / 2 + (1 / 2)sqrt(5)', 'src/real-algebra.mjs~RealAlgebra-example0_34', false)

// in-place evaluation of the absolute value
a = ralg.iadd(ralg.num(1, 2), ralg.num(-1, 2, 5))
// new object is not generated
a = ralg.iabs(a)
testDriver.test(() => { return a.toString() }, '-1 / 2 + (1 / 2)sqrt(5)', 'src/real-algebra.mjs~RealAlgebra-example0_35', false)

// JSON (stringify and parse)
a = ralg.iadd(ralg.num(1, 2), ralg.num(1, 2, 5))
let str = JSON.stringify(a)
b = JSON.parse(str, ralg.reviver)
testDriver.test(() => { return ralg.eq(a, b) }, true, 'src/real-algebra.mjs~RealAlgebra-example0_36', false)
