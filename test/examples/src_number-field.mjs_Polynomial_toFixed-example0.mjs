import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { Monomial as M, Polynomial as P } from '../../src/index.mjs'

let a = new P(new M(1, 2), new M(1, 2, 5))

testDriver.test(() => { return a.toFixed(3) }, '1.618', 'src/number-field.mjs~Polynomial#toFixed-example0_0', false)
