import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import bigInt from 'big-integer'
import { RealAlgebra, ExactRealAlgebra } from '../../src/index.mjs'
let ralg = new RealAlgebra()
let eralg = new ExactRealAlgebra()

testDriver.test(() => { return ralg.isFinite(ralg.num(1, 2)) }, true, 'src/real-algebra.mjs~RealAlgebra#isFinite-example0_0', false)
testDriver.test(() => { return ralg.isFinite(ralg.num(1e999, 2)) }, false, 'src/real-algebra.mjs~RealAlgebra#isFinite-example0_1', false)

testDriver.test(() => { return eralg.isFinite(eralg.num(1, 2)) }, true, 'src/real-algebra.mjs~RealAlgebra#isFinite-example0_2', false)
testDriver.test(() => { return eralg.isFinite(eralg.num(bigInt('1e999'), 2)) }, true, 'src/real-algebra.mjs~RealAlgebra#isFinite-example0_3', false)
