import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { Monomial as M, Polynomial as P } from '../../src/index.mjs'

let a = new P(new M(1, 2, 3))
let b = new P(new M(1, 2, 5))
a.isub(b)

testDriver.test(() => { return a.equals(new P(new M(1, 2, 3), new M(-1, 2, 5))) }, true, 'src/number-field.mjs~Polynomial#isub-example2_0', false)
