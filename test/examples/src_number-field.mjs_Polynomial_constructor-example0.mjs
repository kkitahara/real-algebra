import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { Monomial as M, Polynomial as P } from '../../src/index.mjs'

let a = new P()

testDriver.test(() => { return a instanceof P }, true, 'src/number-field.mjs~Polynomial#constructor-example0_0', false)
testDriver.test(() => { return a.length === 0 }, true, 'src/number-field.mjs~Polynomial#constructor-example0_1', false)
testDriver.test(() => { return a.isZero() }, true, 'src/number-field.mjs~Polynomial#constructor-example0_2', false)

let b = new P(new M(1, 2))

testDriver.test(() => { return b instanceof P }, true, 'src/number-field.mjs~Polynomial#constructor-example0_3', false)
testDriver.test(() => { return b.length === 1 }, true, 'src/number-field.mjs~Polynomial#constructor-example0_4', false)

let c = new P(new M(1, 2), new M(1, 2, 5))

testDriver.test(() => { return c instanceof P }, true, 'src/number-field.mjs~Polynomial#constructor-example0_5', false)
testDriver.test(() => { return c.length === 2 }, true, 'src/number-field.mjs~Polynomial#constructor-example0_6', false)

// It throws an error
testDriver.test(() => { return new P(1) }, Error, 'src/number-field.mjs~Polynomial#constructor-example0_7', false)
