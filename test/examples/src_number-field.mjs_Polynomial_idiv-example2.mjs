import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { Monomial as M, Polynomial as P } from '../../src/index.mjs'

let a = new P(new M(1))
let b = new P(new M(1, 1, 10), new M(1, 1, 6))
a.idiv(b)

testDriver.test(() => { return a.equals(new P(new M(1, 4, 10), new M(-1, 4, 6))) }, true, 'src/number-field.mjs~Polynomial#idiv-example2_0', false)
