import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { RealAlgebra, ExactRealAlgebra } from '../../src/index.mjs'
let ralg = new RealAlgebra()
let ralg2 = new RealAlgebra(0.5)
let eralg = new ExactRealAlgebra()

testDriver.test(() => { return ralg.isNegative(ralg.num(-0, 2)) }, false, 'src/real-algebra.mjs~RealAlgebra#isNegative-example0_0', false)
testDriver.test(() => { return ralg.isNegative(ralg.num(0, 2)) }, false, 'src/real-algebra.mjs~RealAlgebra#isNegative-example0_1', false)
testDriver.test(() => { return ralg.isNegative(ralg.num(1, 2)) }, false, 'src/real-algebra.mjs~RealAlgebra#isNegative-example0_2', false)
testDriver.test(() => { return ralg.isNegative(ralg.num(-1, 2)) }, true, 'src/real-algebra.mjs~RealAlgebra#isNegative-example0_3', false)

// Equality can be controlled
testDriver.test(() => { return ralg2.isNegative(ralg2.num(-0, 2)) }, false, 'src/real-algebra.mjs~RealAlgebra#isNegative-example0_4', false)
testDriver.test(() => { return ralg2.isNegative(ralg2.num(0, 2)) }, false, 'src/real-algebra.mjs~RealAlgebra#isNegative-example0_5', false)
testDriver.test(() => { return ralg2.isNegative(ralg2.num(1, 2)) }, false, 'src/real-algebra.mjs~RealAlgebra#isNegative-example0_6', false)
testDriver.test(() => { return ralg2.isNegative(ralg2.num(-1, 2)) }, false, 'src/real-algebra.mjs~RealAlgebra#isNegative-example0_7', false)

testDriver.test(() => { return eralg.isNegative(eralg.num(0, 2)) }, false, 'src/real-algebra.mjs~RealAlgebra#isNegative-example0_8', false)
testDriver.test(() => { return eralg.isNegative(eralg.num(-0, 2)) }, false, 'src/real-algebra.mjs~RealAlgebra#isNegative-example0_9', false)
testDriver.test(() => { return eralg.isNegative(eralg.num(1, 2)) }, false, 'src/real-algebra.mjs~RealAlgebra#isNegative-example0_10', false)
testDriver.test(() => { return eralg.isNegative(eralg.num(-1, 2)) }, true, 'src/real-algebra.mjs~RealAlgebra#isNegative-example0_11', false)
