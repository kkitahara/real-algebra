import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { Monomial as M } from '../../src/index.mjs'

let a = new M(1, 2, 3)
let b = a.imul(new M(2, 3, 3))

testDriver.test(() => { return b.equals(new M(1, 1, 1)) }, true, 'src/number-field.mjs~Monomial#imul-example1_0', false)

testDriver.test(() => { return a.imul(null) }, Error, 'src/number-field.mjs~Monomial#imul-example1_1', false)
