import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { Monomial as M } from '../../src/index.mjs'

testDriver.test(() => { return new M(1, 2, 3).isPositive() }, true, 'src/number-field.mjs~Monomial#isPositive-example0_0', false)
testDriver.test(() => { return new M(0, 2, 3).isPositive() }, false, 'src/number-field.mjs~Monomial#isPositive-example0_1', false)
testDriver.test(() => { return new M(1, 2, 0).isPositive() }, false, 'src/number-field.mjs~Monomial#isPositive-example0_2', false)
testDriver.test(() => { return new M(1, -2, 3).isPositive() }, false, 'src/number-field.mjs~Monomial#isPositive-example0_3', false)
