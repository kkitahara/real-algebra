import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { Monomial as M } from '../../src/index.mjs'

let a = new M(1, -2, 5)

let b = a
testDriver.test(() => { return b instanceof M }, true, 'src/number-field.mjs~Monomial#copy-example0_0', false)
testDriver.test(() => { return b === a }, true, 'src/number-field.mjs~Monomial#copy-example0_1', false)
testDriver.test(() => { return b.equals(a) }, true, 'src/number-field.mjs~Monomial#copy-example0_2', false)

let c = a.copy()
testDriver.test(() => { return c instanceof M }, true, 'src/number-field.mjs~Monomial#copy-example0_3', false)
testDriver.test(() => { return c === b }, false, 'src/number-field.mjs~Monomial#copy-example0_4', false)
testDriver.test(() => { return c.equals(a) }, true, 'src/number-field.mjs~Monomial#copy-example0_5', false)
