import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { RealAlgebra } from '../../src/index.mjs'
let ralg = new RealAlgebra()

let a = ralg.num(1, 2)
let b = ralg.num(-1, 2)

// GOOD-PRACTICE!
a = ralg.iabs(a)
b = ralg.iabs(b)
testDriver.test(() => { return typeof a === 'number' }, true, 'src/real-algebra.mjs~RealAlgebra#iabs-example1_0', false)
testDriver.test(() => { return typeof b === 'number' }, true, 'src/real-algebra.mjs~RealAlgebra#iabs-example1_1', false)
testDriver.test(() => { return ralg.eq(a, ralg.num(1, 2)) }, true, 'src/real-algebra.mjs~RealAlgebra#iabs-example1_2', false)
testDriver.test(() => { return ralg.eq(b, ralg.num(1, 2)) }, true, 'src/real-algebra.mjs~RealAlgebra#iabs-example1_3', false)
