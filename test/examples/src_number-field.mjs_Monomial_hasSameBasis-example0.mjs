import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { Monomial as M } from '../../src/index.mjs'

let a = new M(1, -2, 3)

testDriver.test(() => { return a.hasSameBasis(new M(2, -3, 3)) }, true, 'src/number-field.mjs~Monomial#hasSameBasis-example0_0', false)
testDriver.test(() => { return a.hasSameBasis(new M(2, -3, 4)) }, false, 'src/number-field.mjs~Monomial#hasSameBasis-example0_1', false)
testDriver.test(() => { return a.hasSameBasis(null) }, Error, 'src/number-field.mjs~Monomial#hasSameBasis-example0_2', false)
