import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { Monomial as M, Polynomial as P } from '../../src/index.mjs'

testDriver.test(() => { return new P(new M(0, 2, 3), new M(0, 2, 5)).isZero() }, true, 'src/number-field.mjs~Polynomial#isZero-example0_0', false)
testDriver.test(() => { return new P(new M(0, 2, 3), new M(1, 2, 5)).isZero() }, false, 'src/number-field.mjs~Polynomial#isZero-example0_1', false)
testDriver.test(() => { return new P().isZero() }, true, 'src/number-field.mjs~Polynomial#isZero-example0_2', false)
