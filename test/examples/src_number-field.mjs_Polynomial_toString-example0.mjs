import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { Monomial as M, Polynomial as P } from '../../src/index.mjs'

let a = new P(new M(-1, 2), new M(1, 2, 5))

testDriver.test(() => { return a.toString() }, '-1 / 2 + (1 / 2)sqrt(5)', 'src/number-field.mjs~Polynomial#toString-example0_0', false)
testDriver.test(() => { return a.toString(2) }, '-1 / 10 + (1 / 10)sqrt(101)', 'src/number-field.mjs~Polynomial#toString-example0_1', false)

testDriver.test(() => { return new P().toString() }, '0', 'src/number-field.mjs~Polynomial#toString-example0_2', false)
