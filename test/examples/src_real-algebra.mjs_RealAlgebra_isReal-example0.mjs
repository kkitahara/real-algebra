import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { RealAlgebra, ExactRealAlgebra } from '../../src/index.mjs'
let ralg = new RealAlgebra()
let eralg = new ExactRealAlgebra()

testDriver.test(() => { return ralg.isReal() }, true, 'src/real-algebra.mjs~RealAlgebra#isReal-example0_0', false)
testDriver.test(() => { return eralg.isReal() }, true, 'src/real-algebra.mjs~RealAlgebra#isReal-example0_1', false)
