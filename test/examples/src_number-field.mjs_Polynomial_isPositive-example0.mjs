import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { Monomial as M, Polynomial as P } from '../../src/index.mjs'

testDriver.test(() => { return new P(new M(1, 2, 5)).isPositive() }, true, 'src/number-field.mjs~Polynomial#isPositive-example0_0', false)
testDriver.test(() => { return new P(new M(-1, 2, 5)).isPositive() }, false, 'src/number-field.mjs~Polynomial#isPositive-example0_1', false)
testDriver.test(() => { return new P().isPositive() }, false, 'src/number-field.mjs~Polynomial#isPositive-example0_2', false)
testDriver.test(() => { return new P(new M(-1, 2, 3), new M(1, 2, 5)).isPositive() }, true, 'src/number-field.mjs~Polynomial#isPositive-example0_3', false)
testDriver.test(() => { return new P(new M(1, 2, 3), new M(-1, 2, 5)).isPositive() }, false, 'src/number-field.mjs~Polynomial#isPositive-example0_4', false)
testDriver.test(() => { return new P(new M(1, 2, 3), new M(-1, 2, 5), new M(1, 2, 1)).isPositive() }, true, 'src/number-field.mjs~Polynomial#isPositive-example0_5', false)
testDriver.test(() => { return new P(new M(1, 1, 6), new M(-1, 1, 10)).isPositive() }, false, 'src/number-field.mjs~Polynomial#isPositive-example0_6', false)
