import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { RealAlgebra } from '../../src/index.mjs'
let ralg = new RealAlgebra()

let a = ralg.num(1, 2, 3)
let s = JSON.stringify(a)
let b = JSON.parse(s, ralg.reviver)

testDriver.test(() => { return typeof s }, 'string', 'src/real-algebra.mjs~RealAlgebra#reviver-example0_0', false)
testDriver.test(() => { return ralg.eq(a, b) }, true, 'src/real-algebra.mjs~RealAlgebra#reviver-example0_1', false)
testDriver.test(() => { return a === b }, true, 'src/real-algebra.mjs~RealAlgebra#reviver-example0_2', false)
