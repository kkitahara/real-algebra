import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { Monomial as M, Polynomial as P } from '../../src/index.mjs'

let a = new P(new M(-1, 2), new M(1, 2, 5))
let s = JSON.stringify(a)

let b = JSON.parse(s, P.reviver)

testDriver.test(() => { return typeof s }, 'string', 'src/number-field.mjs~Polynomial.reviver-example0_0', false)
testDriver.test(() => { return a.equals(b) }, true, 'src/number-field.mjs~Polynomial.reviver-example0_1', false)
testDriver.test(() => { return a === b }, false, 'src/number-field.mjs~Polynomial.reviver-example0_2', false)

let s2 = s.replace('1.0.0', '0.0.0')
testDriver.test(() => { return JSON.parse(s2, P.reviver) }, Error, 'src/number-field.mjs~Polynomial.reviver-example0_3', false)
