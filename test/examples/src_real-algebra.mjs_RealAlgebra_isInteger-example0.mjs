import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { RealAlgebra, ExactRealAlgebra } from '../../src/index.mjs'
let ralg = new RealAlgebra()
let ralg2 = new RealAlgebra(0.5)
let eralg = new ExactRealAlgebra()

testDriver.test(() => { return ralg.isInteger(ralg.num(6, 3)) }, true, 'src/real-algebra.mjs~RealAlgebra#isInteger-example0_0', false)
testDriver.test(() => { return ralg.isInteger(ralg.num(6, 3, 2)) }, false, 'src/real-algebra.mjs~RealAlgebra#isInteger-example0_1', false)
testDriver.test(() => { return ralg.isInteger(ralg.num(-0, 2)) }, true, 'src/real-algebra.mjs~RealAlgebra#isInteger-example0_2', false)
testDriver.test(() => { return ralg.isInteger(ralg.num(0, 2)) }, true, 'src/real-algebra.mjs~RealAlgebra#isInteger-example0_3', false)
testDriver.test(() => { return ralg.isInteger(ralg.num(1, 3)) }, false, 'src/real-algebra.mjs~RealAlgebra#isInteger-example0_4', false)
testDriver.test(() => { return ralg.isInteger(ralg.num(-1, 3)) }, false, 'src/real-algebra.mjs~RealAlgebra#isInteger-example0_5', false)
testDriver.test(() => { return ralg.isInteger(ralg.num(1, 3, 0)) }, true, 'src/real-algebra.mjs~RealAlgebra#isInteger-example0_6', false)

// Equality can be controlled
testDriver.test(() => { return ralg2.isInteger(ralg2.num(6, 3)) }, true, 'src/real-algebra.mjs~RealAlgebra#isInteger-example0_7', false)
testDriver.test(() => { return ralg2.isInteger(ralg2.num(6, 3, 2)) }, true, 'src/real-algebra.mjs~RealAlgebra#isInteger-example0_8', false)
testDriver.test(() => { return ralg2.isInteger(ralg2.num(-0, 2)) }, true, 'src/real-algebra.mjs~RealAlgebra#isInteger-example0_9', false)
testDriver.test(() => { return ralg2.isInteger(ralg2.num(0, 2)) }, true, 'src/real-algebra.mjs~RealAlgebra#isInteger-example0_10', false)
testDriver.test(() => { return ralg2.isInteger(ralg2.num(1, 3)) }, true, 'src/real-algebra.mjs~RealAlgebra#isInteger-example0_11', false)
testDriver.test(() => { return ralg2.isInteger(ralg2.num(-1, 3)) }, true, 'src/real-algebra.mjs~RealAlgebra#isInteger-example0_12', false)
testDriver.test(() => { return ralg2.isInteger(ralg2.num(1, 3, 0)) }, true, 'src/real-algebra.mjs~RealAlgebra#isInteger-example0_13', false)

testDriver.test(() => { return eralg.isInteger(eralg.num(6, 3)) }, true, 'src/real-algebra.mjs~RealAlgebra#isInteger-example0_14', false)
testDriver.test(() => { return eralg.isInteger(eralg.num(6, 3, 2)) }, false, 'src/real-algebra.mjs~RealAlgebra#isInteger-example0_15', false)
testDriver.test(() => { return eralg.isInteger(eralg.num(0, 2)) }, true, 'src/real-algebra.mjs~RealAlgebra#isInteger-example0_16', false)
testDriver.test(() => { return eralg.isInteger(eralg.num(-0, 2)) }, true, 'src/real-algebra.mjs~RealAlgebra#isInteger-example0_17', false)
testDriver.test(() => { return eralg.isInteger(eralg.num(1, 3)) }, false, 'src/real-algebra.mjs~RealAlgebra#isInteger-example0_18', false)
testDriver.test(() => { return eralg.isInteger(eralg.num(-1, 3)) }, false, 'src/real-algebra.mjs~RealAlgebra#isInteger-example0_19', false)
testDriver.test(() => { return eralg.isInteger(eralg.num(1, 3, 0)) }, true, 'src/real-algebra.mjs~RealAlgebra#isInteger-example0_20', false)
