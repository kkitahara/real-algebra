import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { RealAlgebra } from '../../src/index.mjs'
let ralg = new RealAlgebra()

let a = ralg.num(1, 2)
let b = ralg.num(1, 4)

// In-place operation does not work for built-in numbers
// ANTI-PATTERN!
ralg.idiv(a, b)
testDriver.test(() => { return typeof a === 'number' }, true, 'src/real-algebra.mjs~RealAlgebra#idiv-example0_0', false)
testDriver.test(() => { return ralg.eq(a, ralg.num(2)) }, false, 'src/real-algebra.mjs~RealAlgebra#idiv-example0_1', false)
