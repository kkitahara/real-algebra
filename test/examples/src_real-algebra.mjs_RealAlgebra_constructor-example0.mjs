import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { RealAlgebra } from '../../src/index.mjs'

// parameter must be a number
testDriver.test(() => { return new RealAlgebra(null) }, Error, 'src/real-algebra.mjs~RealAlgebra#constructor-example0_0', false)

// parameter must not be a negative number
testDriver.test(() => { return new RealAlgebra(-0.1) }, Error, 'src/real-algebra.mjs~RealAlgebra#constructor-example0_1', false)
