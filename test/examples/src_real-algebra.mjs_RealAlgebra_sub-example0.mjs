import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { RealAlgebra } from '../../src/index.mjs'
let ralg = new RealAlgebra()

let a = ralg.num(1, 2)
let b = ralg.num(1, 4)

let c = ralg.sub(a, b)
testDriver.test(() => { return typeof c === 'number' }, true, 'src/real-algebra.mjs~RealAlgebra#sub-example0_0', false)
testDriver.test(() => { return c !== a }, true, 'src/real-algebra.mjs~RealAlgebra#sub-example0_1', false)
testDriver.test(() => { return ralg.eq(a, ralg.num(1, 2)) }, true, 'src/real-algebra.mjs~RealAlgebra#sub-example0_2', false)
testDriver.test(() => { return ralg.eq(c, ralg.num(1, 4)) }, true, 'src/real-algebra.mjs~RealAlgebra#sub-example0_3', false)
