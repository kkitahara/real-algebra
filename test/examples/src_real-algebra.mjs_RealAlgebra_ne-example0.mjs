import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { RealAlgebra, ExactRealAlgebra } from '../../src/index.mjs'
let ralg = new RealAlgebra()
let ralg2 = new RealAlgebra(0.5)
let eralg = new ExactRealAlgebra()

testDriver.test(() => { return ralg.ne(ralg.num(1, 2), ralg.num(1, 2)) }, false, 'src/real-algebra.mjs~RealAlgebra#ne-example0_0', false)
testDriver.test(() => { return ralg.ne(ralg.num(1, 2), ralg.num(2, 3)) }, true, 'src/real-algebra.mjs~RealAlgebra#ne-example0_1', false)

// Equality can be controlled
testDriver.test(() => { return ralg2.ne(ralg2.num(1, 2), ralg2.num(1, 2)) }, false, 'src/real-algebra.mjs~RealAlgebra#ne-example0_2', false)
testDriver.test(() => { return ralg2.ne(ralg2.num(1, 2), ralg2.num(2, 3)) }, false, 'src/real-algebra.mjs~RealAlgebra#ne-example0_3', false)

testDriver.test(() => { return eralg.ne(eralg.num(1, 2), eralg.num(1, 2)) }, false, 'src/real-algebra.mjs~RealAlgebra#ne-example0_4', false)
testDriver.test(() => { return eralg.ne(eralg.num(1, 2), eralg.num(2, 3)) }, true, 'src/real-algebra.mjs~RealAlgebra#ne-example0_5', false)
