import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { Monomial as M } from '../../src/index.mjs'

let a = new M(1, -2, 2)
let b = new M(2, -3, 3)
let c = new M(1, -2, 3)

// a.sqBasis < b.sqBasis
testDriver.test(() => { return a.compareBasis(b) }, -1, 'src/number-field.mjs~Monomial#compareBasis-example0_0', false)

// c.sqBasis == b.sqBasis
testDriver.test(() => { return c.compareBasis(b) }, 0, 'src/number-field.mjs~Monomial#compareBasis-example0_1', false)

// b.sqBasis > a.sqBasis
testDriver.test(() => { return b.compareBasis(a) }, 1, 'src/number-field.mjs~Monomial#compareBasis-example0_2', false)

testDriver.test(() => { return a.compareBasis(null) }, Error, 'src/number-field.mjs~Monomial#compareBasis-example0_3', false)
