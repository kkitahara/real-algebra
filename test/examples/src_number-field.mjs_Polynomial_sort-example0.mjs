import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { Monomial as M, Polynomial as P } from '../../src/index.mjs'

let a = new P(new M(1, 2, 5))
a.push(new M(1, 2))

testDriver.test(() => { return a[0].compareBasis(a[1]) }, 1, 'src/number-field.mjs~Polynomial#sort-example0_0', false)

a.sort()

testDriver.test(() => { return a[0].compareBasis(a[1]) }, -1, 'src/number-field.mjs~Polynomial#sort-example0_1', false)

// Automatically sorted by the constructor
let b = new P(new M(1, 2, 5), new M(1, 2))

testDriver.test(() => { return b[0].compareBasis(b[1]) }, -1, 'src/number-field.mjs~Polynomial#sort-example0_2', false)
