import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { Monomial as M, Polynomial as P } from '../../src/index.mjs'

let a = new P(new M(1, 2, 1), new M(1, 2, 5))
let b = a
a.idiv(a)

testDriver.test(() => { return a === b }, true, 'src/number-field.mjs~Polynomial#idiv-example1_0', false)
testDriver.test(() => { return a.equals(new P(new M(1))) }, true, 'src/number-field.mjs~Polynomial#idiv-example1_1', false)
testDriver.test(() => { return b.equals(new P(new M(1))) }, true, 'src/number-field.mjs~Polynomial#idiv-example1_2', false)

testDriver.test(() => { return a.idiv(null) }, Error, 'src/number-field.mjs~Polynomial#idiv-example1_3', false)
testDriver.test(() => { return a.idiv(P.zero()) }, Error, 'src/number-field.mjs~Polynomial#idiv-example1_4', false)
