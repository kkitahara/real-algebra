import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { Monomial as M, Polynomial as P } from '../../src/index.mjs'

let a = new P(new M(-1, 2, 3), new M(1, 2, 5))
let b = new P(new M(1, 2, 3), new M(-1, 2, 5))
let c = new P(new M(-1, 2, 3), new M(1, 2, 5))

testDriver.test(() => { return a.compare(b) }, 1, 'src/number-field.mjs~Polynomial#compare-example0_0', false)
testDriver.test(() => { return b.compare(a) }, -1, 'src/number-field.mjs~Polynomial#compare-example0_1', false)
testDriver.test(() => { return a.compare(c) }, 0, 'src/number-field.mjs~Polynomial#compare-example0_2', false)
testDriver.test(() => { return a.compare(null) }, Error, 'src/number-field.mjs~Polynomial#compare-example0_3', false)
