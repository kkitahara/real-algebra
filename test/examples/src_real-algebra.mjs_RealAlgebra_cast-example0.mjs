import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { RealAlgebra, ExactRealAlgebra, Polynomial as P }
  from '../../src/index.mjs'
let ralg = new RealAlgebra()
let eralg = new ExactRealAlgebra()

testDriver.test(() => { return ralg.cast() }, 0, 'src/real-algebra.mjs~RealAlgebra#cast-example0_0', false)
testDriver.test(() => { return ralg.cast(1) }, 1, 'src/real-algebra.mjs~RealAlgebra#cast-example0_1', false)
testDriver.test(() => { return ralg.cast(true) }, 1, 'src/real-algebra.mjs~RealAlgebra#cast-example0_2', false)
testDriver.test(() => { return ralg.cast(false) }, 0, 'src/real-algebra.mjs~RealAlgebra#cast-example0_3', false)

let a = eralg.num(1)
let b = a
testDriver.test(() => { return b === eralg.cast(a) }, true, 'src/real-algebra.mjs~RealAlgebra#cast-example0_4', false)
testDriver.test(() => { return eralg.cast(1) instanceof P }, true, 'src/real-algebra.mjs~RealAlgebra#cast-example0_5', false)
testDriver.test(() => { return b === eralg.cast(1) }, false, 'src/real-algebra.mjs~RealAlgebra#cast-example0_6', false)
testDriver.test(() => { return eralg.eq(b, eralg.cast(1)) }, true, 'src/real-algebra.mjs~RealAlgebra#cast-example0_7', false)
testDriver.test(() => { return eralg.eq(eralg.cast(), 0) }, true, 'src/real-algebra.mjs~RealAlgebra#cast-example0_8', false)
