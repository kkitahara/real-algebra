import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { ExactRealAlgebra as RealAlgebra, Polynomial as P }
  from '../../src/index.mjs'
let ralg = new RealAlgebra()

let a = ralg.num(1, 2)
let b = ralg.num(-1, 2)
let c = a
let d = b

// GOOD-PRACTICE!
a = ralg.iabs(a)
b = ralg.iabs(b)
testDriver.test(() => { return a === c }, true, 'src/real-algebra.mjs~RealAlgebra#iabs-example2_0', false)
testDriver.test(() => { return b === d }, true, 'src/real-algebra.mjs~RealAlgebra#iabs-example2_1', false)
testDriver.test(() => { return typeof a === 'number' }, false, 'src/real-algebra.mjs~RealAlgebra#iabs-example2_2', false)
testDriver.test(() => { return typeof b === 'number' }, false, 'src/real-algebra.mjs~RealAlgebra#iabs-example2_3', false)
testDriver.test(() => { return a instanceof P }, true, 'src/real-algebra.mjs~RealAlgebra#iabs-example2_4', false)
testDriver.test(() => { return b instanceof P }, true, 'src/real-algebra.mjs~RealAlgebra#iabs-example2_5', false)
testDriver.test(() => { return ralg.eq(a, ralg.num(1, 2)) }, true, 'src/real-algebra.mjs~RealAlgebra#iabs-example2_6', false)
testDriver.test(() => { return ralg.eq(b, ralg.num(1, 2)) }, true, 'src/real-algebra.mjs~RealAlgebra#iabs-example2_7', false)
