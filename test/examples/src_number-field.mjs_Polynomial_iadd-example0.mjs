import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { Monomial as M, Polynomial as P } from '../../src/index.mjs'

let a = new P(new M(-1, 2, 3), new M(1, 2, 5))
let b = new P(new M(1, 2, 3), new M(-1, 2, 5))
a.iadd(b)

testDriver.test(() => { return b.equals(P.zero()) }, false, 'src/number-field.mjs~Polynomial#iadd-example0_0', false)
testDriver.test(() => { return a.equals(P.zero()) }, true, 'src/number-field.mjs~Polynomial#iadd-example0_1', false)

a = new P(new M(-1, 2, 3), new M(1, 2, 5))
a[0].compareBasis = () => 2
testDriver.test(() => { return a.iadd(b) }, Error, 'src/number-field.mjs~Polynomial#iadd-example0_2', false)
