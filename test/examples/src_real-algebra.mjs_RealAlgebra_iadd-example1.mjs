import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { RealAlgebra } from '../../src/index.mjs'
let ralg = new RealAlgebra()

let a = ralg.num(1, 2)
let b = ralg.num(1, 4)

// GOOD-PRACTICE!
a = ralg.iadd(a, b)
testDriver.test(() => { return typeof a === 'number' }, true, 'src/real-algebra.mjs~RealAlgebra#iadd-example1_0', false)
testDriver.test(() => { return ralg.eq(a, ralg.num(3, 4)) }, true, 'src/real-algebra.mjs~RealAlgebra#iadd-example1_1', false)
