import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { Polynomial as P } from '../../src/index.mjs'

let a = P.zero()

testDriver.test(() => { return a instanceof P }, true, 'src/number-field.mjs~Polynomial.zero-example0_0', false)
testDriver.test(() => { return a.length === 0 }, true, 'src/number-field.mjs~Polynomial.zero-example0_1', false)
testDriver.test(() => { return a.isZero() }, true, 'src/number-field.mjs~Polynomial.zero-example0_2', false)
