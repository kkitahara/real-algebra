import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { ExactRealAlgebra as RealAlgebra, Polynomial as P }
  from '../../src/index.mjs'
let ralg = new RealAlgebra()

let a = ralg.num(1, 2)
let b = ralg.num(1, 4)

let c = ralg.div(a, b)
testDriver.test(() => { return typeof c === 'number' }, false, 'src/real-algebra.mjs~RealAlgebra#div-example1_0', false)
testDriver.test(() => { return c instanceof P }, true, 'src/real-algebra.mjs~RealAlgebra#div-example1_1', false)
testDriver.test(() => { return c !== a }, true, 'src/real-algebra.mjs~RealAlgebra#div-example1_2', false)
testDriver.test(() => { return ralg.eq(a, ralg.num(1, 2)) }, true, 'src/real-algebra.mjs~RealAlgebra#div-example1_3', false)
testDriver.test(() => { return ralg.eq(c, ralg.num(2)) }, true, 'src/real-algebra.mjs~RealAlgebra#div-example1_4', false)
