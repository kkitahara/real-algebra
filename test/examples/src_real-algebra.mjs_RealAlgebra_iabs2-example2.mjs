import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { ExactRealAlgebra as RealAlgebra, Polynomial as P }
  from '../../src/index.mjs'
let ralg = new RealAlgebra()

let a = ralg.num(-1, 2)
let b = a

// GOOD-PRACTICE!
a = ralg.iabs2(a)
testDriver.test(() => { return a === b }, true, 'src/real-algebra.mjs~RealAlgebra#iabs2-example2_0', false)
testDriver.test(() => { return typeof a === 'number' }, false, 'src/real-algebra.mjs~RealAlgebra#iabs2-example2_1', false)
testDriver.test(() => { return a instanceof P }, true, 'src/real-algebra.mjs~RealAlgebra#iabs2-example2_2', false)
testDriver.test(() => { return ralg.eq(a, ralg.num(1, 4)) }, true, 'src/real-algebra.mjs~RealAlgebra#iabs2-example2_3', false)
testDriver.test(() => { return ralg.eq(b, ralg.num(1, 4)) }, true, 'src/real-algebra.mjs~RealAlgebra#iabs2-example2_4', false)
