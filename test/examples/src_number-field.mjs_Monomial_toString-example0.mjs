import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { Monomial as M } from '../../src/index.mjs'

testDriver.test(() => { return new M(1, 2, 5).toString() }, '(1 / 2)sqrt(5)', 'src/number-field.mjs~Monomial#toString-example0_0', false)
testDriver.test(() => { return new M(1, 2, 5).toString(2) }, '(1 / 10)sqrt(101)', 'src/number-field.mjs~Monomial#toString-example0_1', false)
testDriver.test(() => { return new M(1, 2, 5).toString(10, true) }, ' + (1 / 2)sqrt(5)', 'src/number-field.mjs~Monomial#toString-example0_2', false)
testDriver.test(() => { return new M(-1, 2, 5).toString() }, '-(1 / 2)sqrt(5)', 'src/number-field.mjs~Monomial#toString-example0_3', false)
testDriver.test(() => { return new M(-1, 1, 5).toString(10) }, '-sqrt(5)', 'src/number-field.mjs~Monomial#toString-example0_4', false)
testDriver.test(() => { return new M(-2, 1, 5).toString(10) }, '-2sqrt(5)', 'src/number-field.mjs~Monomial#toString-example0_5', false)
