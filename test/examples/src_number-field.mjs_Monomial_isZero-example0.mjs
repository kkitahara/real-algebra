import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { Monomial as M } from '../../src/index.mjs'

testDriver.test(() => { return new M(0, -2, 3).isZero() }, true, 'src/number-field.mjs~Monomial#isZero-example0_0', false)
testDriver.test(() => { return new M(1, -2, 0).isZero() }, true, 'src/number-field.mjs~Monomial#isZero-example0_1', false)
testDriver.test(() => { return new M(1, -2, 3).isZero() }, false, 'src/number-field.mjs~Monomial#isZero-example0_2', false)
