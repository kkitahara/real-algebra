import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { Monomial as M, Polynomial as P } from '../../src/index.mjs'

let a = new P(new M(1, 2), new M(1, 2, 5))
let b = a.slice()

testDriver.test(() => { return a.length === 2 }, true, 'src/number-field.mjs~Polynomial.[Symbol.species]-example0_0', false)
testDriver.test(() => { return b.length === 2 }, true, 'src/number-field.mjs~Polynomial.[Symbol.species]-example0_1', false)
testDriver.test(() => { return a === b }, false, 'src/number-field.mjs~Polynomial.[Symbol.species]-example0_2', false)
testDriver.test(() => { return a.constructor === P }, true, 'src/number-field.mjs~Polynomial.[Symbol.species]-example0_3', false)
testDriver.test(() => { return b.constructor === P }, false, 'src/number-field.mjs~Polynomial.[Symbol.species]-example0_4', false)
testDriver.test(() => { return b.constructor === Array }, true, 'src/number-field.mjs~Polynomial.[Symbol.species]-example0_5', false)
