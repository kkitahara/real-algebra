import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { Monomial as M, Polynomial as P } from '../../src/index.mjs'

let a = new P(new M(1, 2), new M(1, 2, 5))
let tau = (1 + Math.sqrt(5)) / 2

testDriver.test(() => { return a.valueOf() }, tau, 'src/number-field.mjs~Polynomial#valueOf-example0_0', false)
testDriver.test(() => { return 0 + a }, tau, 'src/number-field.mjs~Polynomial#valueOf-example0_1', false)
