import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { Monomial as M } from '../../src/index.mjs'

let a = new M(1, -2, 3)

testDriver.test(() => { return a.equals(new M(-1, 2, 3)) }, true, 'src/number-field.mjs~Monomial#equals-example0_0', false)
testDriver.test(() => { return a.equals(new M(2, -3, 3)) }, false, 'src/number-field.mjs~Monomial#equals-example0_1', false)
testDriver.test(() => { return a.equals(new M(1, -2, 2)) }, false, 'src/number-field.mjs~Monomial#equals-example0_2', false)
testDriver.test(() => { return a.equals(null) }, Error, 'src/number-field.mjs~Monomial#equals-example0_3', false)
