import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { Monomial as M } from '../../src/index.mjs'

let a = new M(1, 2, 2)
let b = a.idiv(new M(2, 3, 3))

testDriver.test(() => { return b === a }, true, 'src/number-field.mjs~Monomial#idiv-example0_0', false)
testDriver.test(() => { return a.equals(new M(1, 4, 6)) }, true, 'src/number-field.mjs~Monomial#idiv-example0_1', false)
testDriver.test(() => { return b.equals(new M(1, 4, 6)) }, true, 'src/number-field.mjs~Monomial#idiv-example0_2', false)
