import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { RealAlgebra, ExactRealAlgebra } from '../../src/index.mjs'
let ralg = new RealAlgebra()
let eralg = new ExactRealAlgebra()

testDriver.test(() => { return ralg.isExact() }, false, 'src/real-algebra.mjs~RealAlgebra#isExact-example0_0', false)
testDriver.test(() => { return eralg.isExact() }, true, 'src/real-algebra.mjs~RealAlgebra#isExact-example0_1', false)
