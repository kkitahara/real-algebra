import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { Monomial as M } from '../../src/index.mjs'

let tau = (1 + Math.sqrt(5)) / 2

testDriver.test(() => { return 0.5 + new M(1, 2, 5) }, tau, 'src/number-field.mjs~Monomial#valueOf-example1_0', false)
