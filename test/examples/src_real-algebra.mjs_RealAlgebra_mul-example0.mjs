import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { RealAlgebra } from '../../src/index.mjs'
let ralg = new RealAlgebra()

let a = ralg.num(1, 2)
let b = ralg.num(1, 4)

let c = ralg.mul(a, b)
testDriver.test(() => { return typeof c === 'number' }, true, 'src/real-algebra.mjs~RealAlgebra#mul-example0_0', false)
testDriver.test(() => { return c !== a }, true, 'src/real-algebra.mjs~RealAlgebra#mul-example0_1', false)
testDriver.test(() => { return ralg.eq(a, ralg.num(1, 2)) }, true, 'src/real-algebra.mjs~RealAlgebra#mul-example0_2', false)
testDriver.test(() => { return ralg.eq(c, ralg.num(1, 8)) }, true, 'src/real-algebra.mjs~RealAlgebra#mul-example0_3', false)
