import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { Monomial as M } from '../../src/index.mjs'

testDriver.test(() => { return new M(1, 1, 2).valueOf() }, Math.sqrt(2), 'src/number-field.mjs~Monomial#valueOf-example0_0', false)
