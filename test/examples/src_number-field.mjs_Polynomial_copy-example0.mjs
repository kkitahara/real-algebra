import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { Monomial as M, Polynomial as P } from '../../src/index.mjs'

let a = new P(new M(1, 2), new M(1, 2, 5))
let b = a.copy()

testDriver.test(() => { return b instanceof P }, true, 'src/number-field.mjs~Polynomial#copy-example0_0', false)
testDriver.test(() => { return b.equals(a) }, true, 'src/number-field.mjs~Polynomial#copy-example0_1', false)
testDriver.test(() => { return b !== a }, true, 'src/number-field.mjs~Polynomial#copy-example0_2', false)
