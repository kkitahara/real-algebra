import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { Monomial as M } from '../../src/index.mjs'

let a = new M(1, 2, 3)
let b = a.idiv(new M(2, 3, 3))

testDriver.test(() => { return b.equals(new M(3, 4, 1)) }, true, 'src/number-field.mjs~Monomial#idiv-example1_0', false)

testDriver.test(() => { return a.idiv(null) }, Error, 'src/number-field.mjs~Monomial#idiv-example1_1', false)
testDriver.test(() => { return a.idiv(new M(0, 3, 3)) }, Error, 'src/number-field.mjs~Monomial#idiv-example1_2', false)
