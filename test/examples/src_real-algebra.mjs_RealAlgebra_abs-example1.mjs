import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { ExactRealAlgebra as RealAlgebra, Polynomial as P }
  from '../../src/index.mjs'
let ralg = new RealAlgebra()

let a = ralg.num(1, 2)
let b = ralg.num(-1, 2)

let c = ralg.abs(a)
let d = ralg.abs(b)
testDriver.test(() => { return typeof c === 'number' }, false, 'src/real-algebra.mjs~RealAlgebra#abs-example1_0', false)
testDriver.test(() => { return typeof d === 'number' }, false, 'src/real-algebra.mjs~RealAlgebra#abs-example1_1', false)
testDriver.test(() => { return c instanceof P }, true, 'src/real-algebra.mjs~RealAlgebra#abs-example1_2', false)
testDriver.test(() => { return d instanceof P }, true, 'src/real-algebra.mjs~RealAlgebra#abs-example1_3', false)
testDriver.test(() => { return c !== a }, true, 'src/real-algebra.mjs~RealAlgebra#abs-example1_4', false)
testDriver.test(() => { return d !== b }, true, 'src/real-algebra.mjs~RealAlgebra#abs-example1_5', false)
testDriver.test(() => { return ralg.eq(a, ralg.num(1, 2)) }, true, 'src/real-algebra.mjs~RealAlgebra#abs-example1_6', false)
testDriver.test(() => { return ralg.eq(c, ralg.num(1, 2)) }, true, 'src/real-algebra.mjs~RealAlgebra#abs-example1_7', false)
testDriver.test(() => { return ralg.eq(b, ralg.num(-1, 2)) }, true, 'src/real-algebra.mjs~RealAlgebra#abs-example1_8', false)
testDriver.test(() => { return ralg.eq(d, ralg.num(1, 2)) }, true, 'src/real-algebra.mjs~RealAlgebra#abs-example1_9', false)
