import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { Monomial as M, Polynomial as P } from '../../src/index.mjs'

let a = new P(new M(1, 2), new M(1, 2, 5))

testDriver.test(() => { return a.equals(new P(new M(1, 2), new M(1, 2, 5))) }, true, 'src/number-field.mjs~Polynomial#equals-example0_0', false)
testDriver.test(() => { return a.equals(new P(new M(1, 2), new M(1, 2, 3))) }, false, 'src/number-field.mjs~Polynomial#equals-example0_1', false)
testDriver.test(() => { return a.equals(null) }, Error, 'src/number-field.mjs~Polynomial#equals-example0_2', false)
