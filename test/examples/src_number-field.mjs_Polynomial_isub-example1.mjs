import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { Monomial as M, Polynomial as P } from '../../src/index.mjs'

let a = new P(new M(-1, 2, 3), new M(1, 2, 5))
let b = a
a.isub(a)

testDriver.test(() => { return a === b }, true, 'src/number-field.mjs~Polynomial#isub-example1_0', false)
testDriver.test(() => { return a.equals(P.zero()) }, true, 'src/number-field.mjs~Polynomial#isub-example1_1', false)
testDriver.test(() => { return b.equals(P.zero()) }, true, 'src/number-field.mjs~Polynomial#isub-example1_2', false)

testDriver.test(() => { return a.isub(null) }, Error, 'src/number-field.mjs~Polynomial#isub-example1_3', false)
