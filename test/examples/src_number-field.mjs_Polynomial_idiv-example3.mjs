import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { Monomial as M, Polynomial as P } from '../../src/index.mjs'

let a = new P(new M(1, 1, 1), new M(1, 1, 2))
let b = new P(new M(1, 1, 2))
a.idiv(b)

testDriver.test(() => { return a.equals(new P(new M(1, 1, 1), new M(1, 2, 2))) }, true, 'src/number-field.mjs~Polynomial#idiv-example3_0', false)
