import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { RealAlgebra } from '../../src/index.mjs'
let eps = 0.5
let ralg = new RealAlgebra(eps)

let a = ralg.num(1, 2)
let b = ralg.num(1, 3)
testDriver.test(() => { return ralg.eq(a, b) }, true, 'src/real-algebra.mjs~RealAlgebra-example2_0', false)
testDriver.test(() => { return ralg.isZero(ralg.num(1, 2)) }, true, 'src/real-algebra.mjs~RealAlgebra-example2_1', false)
testDriver.test(() => { return ralg.isZero(ralg.num(2, 3)) }, false, 'src/real-algebra.mjs~RealAlgebra-example2_2', false)
