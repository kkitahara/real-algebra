import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { RealAlgebra } from '../../src/index.mjs'
let ralg = new RealAlgebra()

let a = ralg.num(1, 2)

// GOOD-PRACTICE!
a = ralg.ineg(a)
testDriver.test(() => { return typeof a === 'number' }, true, 'src/real-algebra.mjs~RealAlgebra#ineg-example1_0', false)
testDriver.test(() => { return ralg.eq(a, ralg.num(-1, 2)) }, true, 'src/real-algebra.mjs~RealAlgebra#ineg-example1_1', false)
