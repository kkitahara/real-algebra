import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { Monomial as M, Polynomial as P } from '../../src/index.mjs'

// Monomials of the same basis are merged
let a = new P(new M(1, 4))
a.push(new M(1, 2, 5))
a.push(new M(1, 4))
a.sort().simplify()

testDriver.test(() => { return a.length === 2 }, true, 'src/number-field.mjs~Polynomial#simplify-example0_0', false)
testDriver.test(() => { return a.equals(new P(new M(1, 2), new M(1, 2, 5))) }, true, 'src/number-field.mjs~Polynomial#simplify-example0_1', false)

// A Polynomial must be sorted before the simplification
// ANTI-PATTERN!
let b = new P(new M(1, 4))
b.push(new M(1, 2, 5))
b.push(new M(1, 4))
b.simplify()

// Not merged
testDriver.test(() => { return b.length === 2 }, false, 'src/number-field.mjs~Polynomial#simplify-example0_2', false)
testDriver.test(() => { return b.length === 3 }, true, 'src/number-field.mjs~Polynomial#simplify-example0_3', false)
testDriver.test(() => { return b.equals(new P(new M(1, 2), new M(1, 2, 5))) }, false, 'src/number-field.mjs~Polynomial#simplify-example0_4', false)

// Monomials of zero coefficient are removed
let c = new P(new M(1, 4))
c.push(new M(0, 2, 5))
c.push(new M(1, 4))
c.sort().simplify()

testDriver.test(() => { return c.length === 1 }, true, 'src/number-field.mjs~Polynomial#simplify-example0_5', false)
testDriver.test(() => { return c.equals(new P(new M(1, 2))) }, true, 'src/number-field.mjs~Polynomial#simplify-example0_6', false)

// Automatically sorted and simplified by the constructor
let d = new P(new M(1, 4), new M(0, 2, 5), new M(1, 4))

testDriver.test(() => { return d.length === 1 }, true, 'src/number-field.mjs~Polynomial#simplify-example0_7', false)
testDriver.test(() => { return d.equals(new P(new M(1, 2))) }, true, 'src/number-field.mjs~Polynomial#simplify-example0_8', false)
