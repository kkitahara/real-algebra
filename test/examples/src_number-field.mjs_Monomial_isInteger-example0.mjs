import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { Monomial as M } from '../../src/index.mjs'

testDriver.test(() => { return new M(6, 3, 1).isInteger() }, true, 'src/number-field.mjs~Monomial#isInteger-example0_0', false)
testDriver.test(() => { return new M(0, 2, 3).isInteger() }, true, 'src/number-field.mjs~Monomial#isInteger-example0_1', false)
testDriver.test(() => { return new M(1, 2, 0).isInteger() }, true, 'src/number-field.mjs~Monomial#isInteger-example0_2', false)
testDriver.test(() => { return new M(1, 2, 1).isInteger() }, false, 'src/number-field.mjs~Monomial#isInteger-example0_3', false)
testDriver.test(() => { return new M(2, 1, 1).isInteger() }, true, 'src/number-field.mjs~Monomial#isInteger-example0_4', false)
testDriver.test(() => { return new M(2, 1, 3).isInteger() }, false, 'src/number-field.mjs~Monomial#isInteger-example0_5', false)
