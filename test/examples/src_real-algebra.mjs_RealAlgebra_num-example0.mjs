import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { RealAlgebra, ExactRealAlgebra, Monomial as M, Polynomial as P }
  from '../../src/index.mjs'
let ralg = new RealAlgebra()
let eralg = new ExactRealAlgebra()

testDriver.test(() => { return ralg.num(2, 3, 2) === 2 / 3 * Math.sqrt(2) }, true, 'src/real-algebra.mjs~RealAlgebra#num-example0_0', false)
testDriver.test(() => { return ralg.num(2, 3) === 2 / 3 }, true, 'src/real-algebra.mjs~RealAlgebra#num-example0_1', false)
testDriver.test(() => { return ralg.num(2) === 2 }, true, 'src/real-algebra.mjs~RealAlgebra#num-example0_2', false)
testDriver.test(() => { return ralg.num() === 0 }, true, 'src/real-algebra.mjs~RealAlgebra#num-example0_3', false)
testDriver.test(() => { return ralg.num(2, 0) }, Error, 'src/real-algebra.mjs~RealAlgebra#num-example0_4', false)
testDriver.test(() => { return ralg.num(2, 3, -2) }, Error, 'src/real-algebra.mjs~RealAlgebra#num-example0_5', false)

let a = eralg.num(2, 3, 2)
testDriver.test(() => { return a instanceof P }, true, 'src/real-algebra.mjs~RealAlgebra#num-example0_6', false)
testDriver.test(() => { return a.equals(new P(new M(2, 3, 2))) }, true, 'src/real-algebra.mjs~RealAlgebra#num-example0_7', false)
testDriver.test(() => { return eralg.num(2, 3).equals(new P(new M(2, 3))) }, true, 'src/real-algebra.mjs~RealAlgebra#num-example0_8', false)
testDriver.test(() => { return eralg.num(2).equals(new P(new M(2))) }, true, 'src/real-algebra.mjs~RealAlgebra#num-example0_9', false)
testDriver.test(() => { return eralg.num().equals(new P(new M(0))) }, true, 'src/real-algebra.mjs~RealAlgebra#num-example0_10', false)
testDriver.test(() => { return eralg.num(2, 0) }, Error, 'src/real-algebra.mjs~RealAlgebra#num-example0_11', false)
testDriver.test(() => { return eralg.num(2, 3, -2) }, Error, 'src/real-algebra.mjs~RealAlgebra#num-example0_12', false)
