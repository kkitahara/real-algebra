import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { RealAlgebra, ExactRealAlgebra, Monomial as M, Polynomial as P }
  from '../../src/index.mjs'
let r = new RealAlgebra()
let er = new ExactRealAlgebra()

testDriver.test(() => { return r.$(2, 3, 2) === 2 / 3 * Math.sqrt(2) }, true, 'src/real-algebra.mjs~RealAlgebra#$-example0_0', false)
testDriver.test(() => { return r.$(2, 3) === 2 / 3 }, true, 'src/real-algebra.mjs~RealAlgebra#$-example0_1', false)
testDriver.test(() => { return r.$(2) === 2 }, true, 'src/real-algebra.mjs~RealAlgebra#$-example0_2', false)
testDriver.test(() => { return r.$() === 0 }, true, 'src/real-algebra.mjs~RealAlgebra#$-example0_3', false)
testDriver.test(() => { return r.$(2, 0) }, Error, 'src/real-algebra.mjs~RealAlgebra#$-example0_4', false)
testDriver.test(() => { return r.$(2, 3, -2) }, Error, 'src/real-algebra.mjs~RealAlgebra#$-example0_5', false)

let a = er.$(2, 3, 2)
testDriver.test(() => { return a instanceof P }, true, 'src/real-algebra.mjs~RealAlgebra#$-example0_6', false)
testDriver.test(() => { return a.equals(new P(new M(2, 3, 2))) }, true, 'src/real-algebra.mjs~RealAlgebra#$-example0_7', false)
testDriver.test(() => { return er.$(2, 3).equals(new P(new M(2, 3))) }, true, 'src/real-algebra.mjs~RealAlgebra#$-example0_8', false)
testDriver.test(() => { return er.$(2).equals(new P(new M(2))) }, true, 'src/real-algebra.mjs~RealAlgebra#$-example0_9', false)
testDriver.test(() => { return er.$().equals(new P(new M(0))) }, true, 'src/real-algebra.mjs~RealAlgebra#$-example0_10', false)
testDriver.test(() => { return er.$(2, 0) }, Error, 'src/real-algebra.mjs~RealAlgebra#$-example0_11', false)
testDriver.test(() => { return er.$(2, 3, -2) }, Error, 'src/real-algebra.mjs~RealAlgebra#$-example0_12', false)
