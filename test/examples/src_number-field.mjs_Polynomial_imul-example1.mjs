import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { Monomial as M, Polynomial as P } from '../../src/index.mjs'

let a = new P(new M(1, 2), new M(1, 2, 5))
let b = a
let c = new P(new M(3, 2), new M(1, 2, 5))
a.imul(a)

testDriver.test(() => { return a === b }, true, 'src/number-field.mjs~Polynomial#imul-example1_0', false)
testDriver.test(() => { return a.equals(c) }, true, 'src/number-field.mjs~Polynomial#imul-example1_1', false)
testDriver.test(() => { return b.equals(c) }, true, 'src/number-field.mjs~Polynomial#imul-example1_2', false)

testDriver.test(() => { return a.imul(null) }, Error, 'src/number-field.mjs~Polynomial#imul-example1_3', false)
