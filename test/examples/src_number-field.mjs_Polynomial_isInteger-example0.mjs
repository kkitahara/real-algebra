import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { Monomial as M, Polynomial as P } from '../../src/index.mjs'

testDriver.test(() => { return new P().isInteger() }, true, 'src/number-field.mjs~Polynomial#isInteger-example0_0', false)
testDriver.test(() => { return new P(new M(1, 2, 5)).isInteger() }, false, 'src/number-field.mjs~Polynomial#isInteger-example0_1', false)
testDriver.test(() => { return new P(new M(1, 2, 0)).isInteger() }, true, 'src/number-field.mjs~Polynomial#isInteger-example0_2', false)
testDriver.test(() => { return new P(new M(0, 2, 2)).isInteger() }, true, 'src/number-field.mjs~Polynomial#isInteger-example0_3', false)
testDriver.test(() => { return new P(new M(6, 3, 1)).isInteger() }, true, 'src/number-field.mjs~Polynomial#isInteger-example0_4', false)
testDriver.test(() => { return new P(new M(6, 3, 5), new M(6, -3, 5)).isInteger() }, true, 'src/number-field.mjs~Polynomial#isInteger-example0_5', false)
testDriver.test(() => { return new P(new M(1, 1, 1), new M(6, -3, 5)).isInteger() }, false, 'src/number-field.mjs~Polynomial#isInteger-example0_6', false)
testDriver.test(() => { return new P(new M(6, -3, 5), new M(6, 3, 1)).isInteger() }, false, 'src/number-field.mjs~Polynomial#isInteger-example0_7', false)
testDriver.test(() => { return new P(new M(1, 4, 1), new M(3, 4, 1)).isInteger() }, true, 'src/number-field.mjs~Polynomial#isInteger-example0_8', false)
