import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { RealAlgebra } from '../../src/index.mjs'

let ralg = new RealAlgebra()

testDriver.test(() => { return ralg.ralg === ralg }, true, 'src/real-algebra.mjs~RealAlgebra#ralg-example0_0', false)
