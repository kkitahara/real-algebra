import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import bigInt from 'big-integer'
import bigRat from 'big-rational'
import { Monomial as M } from '../../src/index.mjs'

let a = new M(1, -2, 5)
testDriver.test(() => { return a instanceof M }, true, 'src/number-field.mjs~Monomial#constructor-example0_0', false)
testDriver.test(() => { return a.coef.equals(bigRat(1, -2)) }, true, 'src/number-field.mjs~Monomial#constructor-example0_1', false)
testDriver.test(() => { return a.sqBasis.equals(5) }, true, 'src/number-field.mjs~Monomial#constructor-example0_2', false)

// parameters can be `bigInt`
testDriver.test(() => { return a.equals(new M(bigInt(1), bigInt(-2), bigInt(5))) }, true, 'src/number-field.mjs~Monomial#constructor-example0_3', false)

testDriver.test(() => { return new M(1, -2).equals(new M(1, -2, 1)) }, true, 'src/number-field.mjs~Monomial#constructor-example0_4', false)
testDriver.test(() => { return new M(5).equals(new M(5, 1, 1)) }, true, 'src/number-field.mjs~Monomial#constructor-example0_5', false)
testDriver.test(() => { return new M().equals(new M(0, 1, 1)) }, true, 'src/number-field.mjs~Monomial#constructor-example0_6', false)

let b = new M(1, -2, 0)
testDriver.test(() => { return b instanceof M }, true, 'src/number-field.mjs~Monomial#constructor-example0_7', false)
testDriver.test(() => { return b.coef.equals(bigRat(1, -2)) }, false, 'src/number-field.mjs~Monomial#constructor-example0_8', false)
testDriver.test(() => { return b.sqBasis.equals(0) }, false, 'src/number-field.mjs~Monomial#constructor-example0_9', false)
// if `b` is zero, `coef` is zero and `sqBasis` is one
testDriver.test(() => { return b.coef.equals(0) }, true, 'src/number-field.mjs~Monomial#constructor-example0_10', false)
testDriver.test(() => { return b.sqBasis.equals(1) }, true, 'src/number-field.mjs~Monomial#constructor-example0_11', false)

let c = new M(0, -2, 5)
testDriver.test(() => { return c instanceof M }, true, 'src/number-field.mjs~Monomial#constructor-example0_12', false)
testDriver.test(() => { return c.coef.equals(0) }, true, 'src/number-field.mjs~Monomial#constructor-example0_13', false)
testDriver.test(() => { return c.sqBasis.equals(5) }, false, 'src/number-field.mjs~Monomial#constructor-example0_14', false)
// if `coef` is zero, then `sqBasis` is one
testDriver.test(() => { return c.sqBasis.equals(1) }, true, 'src/number-field.mjs~Monomial#constructor-example0_15', false)

let d = new M(1.1, -2, 1)
testDriver.test(() => { return d instanceof M }, true, 'src/number-field.mjs~Monomial#constructor-example0_16', false)
testDriver.test(() => { return d.coef.equals(bigRat(-11, 20)) }, true, 'src/number-field.mjs~Monomial#constructor-example0_17', false)

// Invalid parameter type
testDriver.test(() => { return new M(null, -2, 1) }, Error, 'src/number-field.mjs~Monomial#constructor-example0_18', false)
testDriver.test(() => { return new M(1, null, 1) }, Error, 'src/number-field.mjs~Monomial#constructor-example0_19', false)
testDriver.test(() => { return new M(1, -2, null) }, Error, 'src/number-field.mjs~Monomial#constructor-example0_20', false)

// Non-integer parameter
testDriver.test(() => { return new M(1, -2.1, 1) }, Error, 'src/number-field.mjs~Monomial#constructor-example0_21', false)
testDriver.test(() => { return new M(1, -2, 1.1) }, Error, 'src/number-field.mjs~Monomial#constructor-example0_22', false)

// `numCoef` is zero
testDriver.test(() => { return new M(1, 0, -1) }, Error, 'src/number-field.mjs~Monomial#constructor-example0_23', false)

// `sqBasis` is negative
testDriver.test(() => { return new M(1, -2, -1) }, Error, 'src/number-field.mjs~Monomial#constructor-example0_24', false)

// `sqBasis` must be square-free, but the constructor does not check it.
// ANTI-PATTERN!
testDriver.test(() => { return new M(1, -2, 4) instanceof M }, true, 'src/number-field.mjs~Monomial#constructor-example0_25', false)
