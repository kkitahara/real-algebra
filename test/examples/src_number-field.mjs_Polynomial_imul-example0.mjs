import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { Monomial as M, Polynomial as P } from '../../src/index.mjs'

let a = new P(new M(1, 2), new M(1, 2, 5))
let b = new P(new M(-1, 2), new M(1, 2, 5))
a.imul(b)

testDriver.test(() => { return b.equals(new P(new M(1))) }, false, 'src/number-field.mjs~Polynomial#imul-example0_0', false)
testDriver.test(() => { return a.equals(new P(new M(1))) }, true, 'src/number-field.mjs~Polynomial#imul-example0_1', false)
