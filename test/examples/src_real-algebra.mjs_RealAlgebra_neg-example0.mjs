import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { RealAlgebra } from '../../src/index.mjs'
let ralg = new RealAlgebra()

let a = ralg.num(1, 2)

let b = ralg.neg(a)
testDriver.test(() => { return typeof b === 'number' }, true, 'src/real-algebra.mjs~RealAlgebra#neg-example0_0', false)
testDriver.test(() => { return b !== a }, true, 'src/real-algebra.mjs~RealAlgebra#neg-example0_1', false)
testDriver.test(() => { return ralg.eq(a, ralg.num(1, 2)) }, true, 'src/real-algebra.mjs~RealAlgebra#neg-example0_2', false)
testDriver.test(() => { return ralg.eq(b, ralg.num(-1, 2)) }, true, 'src/real-algebra.mjs~RealAlgebra#neg-example0_3', false)
