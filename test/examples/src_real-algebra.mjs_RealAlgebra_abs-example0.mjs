import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { RealAlgebra } from '../../src/index.mjs'
let ralg = new RealAlgebra()

let a = ralg.num(1, 2)
let b = ralg.num(-1, 2)

let c = ralg.abs(a)
let d = ralg.abs(b)
testDriver.test(() => { return typeof c === 'number' }, true, 'src/real-algebra.mjs~RealAlgebra#abs-example0_0', false)
testDriver.test(() => { return typeof d === 'number' }, true, 'src/real-algebra.mjs~RealAlgebra#abs-example0_1', false)
testDriver.test(() => { return c !== a }, false, 'src/real-algebra.mjs~RealAlgebra#abs-example0_2', false)
testDriver.test(() => { return d !== b }, true, 'src/real-algebra.mjs~RealAlgebra#abs-example0_3', false)
testDriver.test(() => { return ralg.eq(a, ralg.num(1, 2)) }, true, 'src/real-algebra.mjs~RealAlgebra#abs-example0_4', false)
testDriver.test(() => { return ralg.eq(c, ralg.num(1, 2)) }, true, 'src/real-algebra.mjs~RealAlgebra#abs-example0_5', false)
testDriver.test(() => { return ralg.eq(b, ralg.num(-1, 2)) }, true, 'src/real-algebra.mjs~RealAlgebra#abs-example0_6', false)
testDriver.test(() => { return ralg.eq(d, ralg.num(1, 2)) }, true, 'src/real-algebra.mjs~RealAlgebra#abs-example0_7', false)
