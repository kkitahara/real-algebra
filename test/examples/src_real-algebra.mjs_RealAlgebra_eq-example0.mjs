import testDriver from '@kkitahara/esdoc-examples-test-plugin/src/simple-test-driver.mjs'

import { RealAlgebra, ExactRealAlgebra } from '../../src/index.mjs'
let ralg = new RealAlgebra()
let ralg2 = new RealAlgebra(0.5)
let eralg = new ExactRealAlgebra()

testDriver.test(() => { return ralg.eq(ralg.num(1, 2), ralg.num(1, 2)) }, true, 'src/real-algebra.mjs~RealAlgebra#eq-example0_0', false)
testDriver.test(() => { return ralg.eq(ralg.num(1, 2), ralg.num(2, 3)) }, false, 'src/real-algebra.mjs~RealAlgebra#eq-example0_1', false)

// Equality can be controlled
testDriver.test(() => { return ralg2.eq(ralg2.num(1, 2), ralg2.num(1, 2)) }, true, 'src/real-algebra.mjs~RealAlgebra#eq-example0_2', false)
testDriver.test(() => { return ralg2.eq(ralg2.num(1, 2), ralg2.num(2, 3)) }, true, 'src/real-algebra.mjs~RealAlgebra#eq-example0_3', false)

testDriver.test(() => { return eralg.eq(eralg.num(1, 2), eralg.num(1, 2)) }, true, 'src/real-algebra.mjs~RealAlgebra#eq-example0_4', false)
testDriver.test(() => { return eralg.eq(eralg.num(1, 2), eralg.num(2, 3)) }, false, 'src/real-algebra.mjs~RealAlgebra#eq-example0_5', false)
