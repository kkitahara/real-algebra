/**
 * @source: https://www.npmjs.com/package/big-rational
 * @license magnet:?xt=urn:btih:723febf9f6185544f57f0660a41489c7d6b4931b&dn=wtfpl.txt WTFPL
 */

import bigRat from 'big-rational'
export { bigRat }

/* @license-end */
