/**
 * @source: https://www.npmjs.com/package/@kkitahara/real-algebra
 * @license magnet:?xt=urn:btih:8e4f440f4c65981c5bf93c76d35135ba5064d8b7&dn=apache-2.0.txt Apache-2.0
 */

/**
 * Copyright 2019 Koichi Kitahara
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @typedef {number|Polynomial} RealAlgebraicElement
 *
 * @desc
 * A RealAlgebraicElement denotes a {@link number}
 * or a {@link Polynomial} as follows.
 * * A {@link number} for the numerical algebra (see {@link RealAlgebra}).
 * * A {@link Polynomial} for the exact algebra (see {@link ExactRealAlgebra}).
 */

/**
 * @desc
 * The RealAlgebra class is a reference (numerical) implementation of
 * general classes for real algebra.
 * It uses built-in {@link number}s as {@link RealAlgebraicElement}s.
 *
 * @version 1.0.0
 * @since 1.0.0
 *
 * @example
 * import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
 * let ralg = new RealAlgebra()
 * let a, b, c
 *
 * // generate a new number
 * a = ralg.num(1, 2, 5)
 * a.toString() // '(1 / 2)sqrt(5)'
 *
 * a = ralg.num(1, 2)
 * a.toString() // '1 / 2'
 *
 * a = ralg.num(3)
 * a.toString() // '3'
 *
 * // generate a new number (short form, since v1.2.0)
 * a = ralg.$(1, 2, 5)
 * a.toString() // '(1 / 2)sqrt(5)'
 *
 * a = ralg.$(1, 2)
 * a.toString() // '1 / 2'
 *
 * a = ralg.$(3)
 * a.toString() // '3'
 *
 * // copy (create a new object)
 * a = ralg.num(1, 2, 5)
 * b = ralg.copy(a)
 * b.toString() // '(1 / 2)sqrt(5)'
 *
 * // equality
 * a = ralg.num(1, 2, 5)
 * b = ralg.num(3, 2, 5)
 * ralg.eq(a, b) // false
 *
 * b = ralg.num(1, 2, 5)
 * ralg.eq(a, b) // true
 *
 * // inequality
 * a = ralg.num(1, 2, 5)
 * b = ralg.num(3, 2, 5)
 * ralg.ne(a, b) // true
 *
 * b = ralg.num(1, 2, 5)
 * ralg.ne(a, b) // false
 *
 * // isZero
 * ralg.isZero(ralg.num(0)) // true
 * ralg.isZero(ralg.num(1, 2, 5)) // false
 * ralg.isZero(ralg.num(-1, 2, 5)) // false
 *
 * // isPositive
 * ralg.isPositive(ralg.num(0)) // false
 * ralg.isPositive(ralg.num(1, 2, 5)) // true
 * ralg.isPositive(ralg.num(-1, 2, 5)) // false
 *
 * // isNegative
 * ralg.isNegative(ralg.num(0)) // false
 * ralg.isNegative(ralg.num(1, 2, 5)) // false
 * ralg.isNegative(ralg.num(-1, 2, 5)) // true
 *
 * // isInteger (since v1.1.0)
 * ralg.isInteger(ralg.num(0)) // true
 * ralg.isInteger(ralg.num(6, 3)) // true
 * ralg.isInteger(ralg.num(1, 2)) // false
 * ralg.isInteger(ralg.num(2, 1, 3)) // false
 *
 * // addition
 * a = ralg.num(1, 2, 5)
 * b = ralg.num(1, 2, 1)
 * // new object is generated
 * c = ralg.add(a, b)
 * c.toString() // '1 / 2 + (1 / 2)sqrt(5)'
 *
 * // in-place addition
 * a = ralg.num(1, 2, 5)
 * b = ralg.num(1, 2, 1)
 * // new object is not generated
 * a = ralg.iadd(a, b)
 * a.toString() // '1 / 2 + (1 / 2)sqrt(5)'
 *
 * // subtraction
 * a = ralg.num(1, 2, 5)
 * b = ralg.num(1, 2, 1)
 * // new object is generated
 * c = ralg.sub(a, b)
 * c.toString() // '-1 / 2 + (1 / 2)sqrt(5)'
 *
 * // in-place subtraction
 * a = ralg.num(1, 2, 5)
 * b = ralg.num(1, 2, 1)
 * // new object is not generated
 * a = ralg.isub(a, b)
 * a.toString() // '-1 / 2 + (1 / 2)sqrt(5)'
 *
 * // multiplication
 * a = ralg.iadd(ralg.num(1, 2), ralg.num(1, 2, 5))
 * b = ralg.iadd(ralg.num(-1, 2), ralg.num(1, 2, 5))
 * // new object is generated
 * c = ralg.mul(a, b)
 * c.toString() // '1'
 *
 * // in-place multiplication
 * a = ralg.iadd(ralg.num(1, 2), ralg.num(1, 2, 5))
 * b = ralg.iadd(ralg.num(-1, 2), ralg.num(1, 2, 5))
 * // new object is not generated
 * a = ralg.imul(a, b)
 * a.toString() // '1'
 *
 * // division
 * a = ralg.iadd(ralg.num(1, 2), ralg.num(1, 2, 5))
 * b = ralg.iadd(ralg.num(-1, 2), ralg.num(1, 2, 5))
 * // new object is generated
 * c = ralg.div(a, b)
 * c.toString() // '3 / 2 + (1 / 2)sqrt(5)'
 *
 * // in-place division
 * a = ralg.iadd(ralg.num(1, 2), ralg.num(1, 2, 5))
 * b = ralg.iadd(ralg.num(-1, 2), ralg.num(1, 2, 5))
 * // new object is not generated
 * a = ralg.idiv(a, b)
 * a.toString() // '3 / 2 + (1 / 2)sqrt(5)'
 *
 * // multiplication by -1
 * a = ralg.iadd(ralg.num(1, 2), ralg.num(1, 2, 5))
 * // new object is generated
 * b = ralg.neg(a)
 * b.toString() // '-1 / 2 - (1 / 2)sqrt(5)'
 *
 * // in-place multiplication by -1
 * a = ralg.iadd(ralg.num(1, 2), ralg.num(1, 2, 5))
 * // new object is not generated
 * a = ralg.ineg(a)
 * a.toString() // '-1 / 2 - (1 / 2)sqrt(5)'
 *
 * // absolute value
 * a = ralg.iadd(ralg.num(1, 2), ralg.num(-1, 2, 5))
 * // new object is generated
 * b = ralg.abs(a)
 * b.toString() // '-1 / 2 + (1 / 2)sqrt(5)'
 *
 * // in-place evaluation of the absolute value
 * a = ralg.iadd(ralg.num(1, 2), ralg.num(-1, 2, 5))
 * // new object is not generated
 * a = ralg.iabs(a)
 * a.toString() // '-1 / 2 + (1 / 2)sqrt(5)'
 *
 * // JSON (stringify and parse)
 * a = ralg.iadd(ralg.num(1, 2), ralg.num(1, 2, 5))
 * let str = JSON.stringify(a)
 * b = JSON.parse(str, ralg.reviver)
 * ralg.eq(a, b) // true
 *
 * @example
 * import { RealAlgebra } from '@kkitahara/real-algebra'
 * let ralg = new RealAlgebra()
 *
 * let a = ralg.num(1, 2)
 * let b = ralg.num(1, 3)
 * ralg.eq(a, b) // false
 * ralg.isZero(ralg.num(1, 999999)) // false
 *
 * @example
 * import { RealAlgebra } from '@kkitahara/real-algebra'
 * let eps = 0.5
 * let ralg = new RealAlgebra(eps)
 *
 * let a = ralg.num(1, 2)
 * let b = ralg.num(1, 3)
 * ralg.eq(a, b) // true
 * ralg.isZero(ralg.num(1, 2)) // true
 * ralg.isZero(ralg.num(2, 3)) // false
 */
export class RealAlgebra {
  /**
   * @desc
   * The constructor function of the {@link RealAlgebra} class.
   * The parameter `eps` is only for numerical algebra.
   *
   * @param {number} [eps = 0]
   * a non-negative {@link number}.
   *
   * @throws {Error}
   * if `eps` is not a {@link number}.
   *
   * @throws {Error}
   * if `eps` is negative.
   *
   * @version 1.0.0
   * @since 1.0.0
   *
   * @example
   * import { RealAlgebra } from '@kkitahara/real-algebra'
   *
   * // parameter must be a number
   * new RealAlgebra(null) // Error
   *
   * // parameter must not be a negative number
   * new RealAlgebra(-0.1) // Error
   */
  constructor (eps = 0) {
    if (typeof eps !== 'number') {
      throw Error('type of `eps` must be `number`.')
    }
    if (eps < 0) {
      throw Error('`eps` must not be negative.')
    }
    /**
     * @desc
     * If the difference between two {@link number}s are
     * smaller than or equal to {@link RealAlgebra#eps},
     * then the two {@link number}s are considered to be equal.
     * It is only for numerical algebra.
     *
     * @type {number}
     *
     * @version 1.0.0
     * @since 1.0.0
     */
    this.eps = eps
  }

  /**
   * @desc
   * The RealAlgebra#ralg method returns `this`.
   *
   * @type {RealAlgebra}
   *
   * @version 1.0.0
   * @since 1.0.0
   *
   * @example
   * import { RealAlgebra } from '@kkitahara/real-algebra'
   *
   * let ralg = new RealAlgebra()
   *
   * ralg.ralg === ralg // true
   */
  get ralg () {
    return this
  }

  /**
   * @desc
   * The RealAlgebra#num method returns
   * a {@link RealAlgebraicElement} representing
   * (*p* / *q*)sqrt(*b*),
   * where *p* is an integer, *q* is a non-zero integer,
   * and *b* is a positive, square-free integer.
   *
   * CAUTION: this method does not check if `b` is square-free.
   *
   * @param {number} [p = 0]
   * an integer.
   *
   * @param {number} [q = 1]
   * a non-zero integer (can be negative).
   *
   * @param {number} [b = 1]
   * a positive, square-free integer.
   *
   * @return {RealAlgebraicElement}
   * a {@link RealAlgebraicElement} representing `(p / q)sqrt(b)`.
   *
   * @throws {Error}
   * if `q` is zero.
   *
   * @version 1.0.0
   * @since 1.0.0
   *
   * @example
   * import { RealAlgebra, ExactRealAlgebra, Monomial as M, Polynomial as P }
   *   from '@kkitahara/real-algebra'
   * let ralg = new RealAlgebra()
   * let eralg = new ExactRealAlgebra()
   *
   * ralg.num(2, 3, 2) === 2 / 3 * Math.sqrt(2) // true
   * ralg.num(2, 3) === 2 / 3 // true
   * ralg.num(2) === 2 // true
   * ralg.num() === 0 // true
   * ralg.num(2, 0) // Error
   * ralg.num(2, 3, -2) // Error
   *
   * let a = eralg.num(2, 3, 2)
   * a instanceof P // true
   * a.equals(new P(new M(2, 3, 2))) // true
   * eralg.num(2, 3).equals(new P(new M(2, 3))) // true
   * eralg.num(2).equals(new P(new M(2))) // true
   * eralg.num().equals(new P(new M(0))) // true
   * eralg.num(2, 0) // Error
   * eralg.num(2, 3, -2) // Error
   */
  num (p = 0, q = 1, b = 1) {
    if (q === 0) {
      throw Error('`q` must be non-zero.')
    } else if (b < 0) {
      throw Error('`b` must not be negative.')
    }
    return p / q * Math.sqrt(b)
  }

  /**
   * @desc
   * The RealAlgebra#$ method is an alias for the RealAlgebra#num.
   *
   * @param {number} [p = 0]
   * an integer.
   *
   * @param {number} [q = 1]
   * a non-zero integer (can be negative).
   *
   * @param {number} [b = 1]
   * a positive, square-free integer.
   *
   * @return {RealAlgebraicElement}
   * a {@link RealAlgebraicElement} representing `(p / q)sqrt(b)`.
   *
   * @version 1.2.0
   * @since 1.2.0
   *
   * @example
   * import { RealAlgebra, ExactRealAlgebra, Monomial as M, Polynomial as P }
   *   from '@kkitahara/real-algebra'
   * let r = new RealAlgebra()
   * let er = new ExactRealAlgebra()
   *
   * r.$(2, 3, 2) === 2 / 3 * Math.sqrt(2) // true
   * r.$(2, 3) === 2 / 3 // true
   * r.$(2) === 2 // true
   * r.$() === 0 // true
   * r.$(2, 0) // Error
   * r.$(2, 3, -2) // Error
   *
   * let a = er.$(2, 3, 2)
   * a instanceof P // true
   * a.equals(new P(new M(2, 3, 2))) // true
   * er.$(2, 3).equals(new P(new M(2, 3))) // true
   * er.$(2).equals(new P(new M(2))) // true
   * er.$().equals(new P(new M(0))) // true
   * er.$(2, 0) // Error
   * er.$(2, 3, -2) // Error
   */
  $ (p = 0, q = 1, b = 1) {
    return this.num(p, q, b)
  }

  /**
   * @desc
   * The RealAlgebra#cast method just returns
   * `a` if `a` is a valid {@link RealAlgebraicElement}
   * and otherwise casts `a` to a valid {@link RealAlgebraicElement}.
   *
   * @param {object} [a = 0]
   * an object.
   *
   * @return {RealAlgebraicElement}
   * `a` or a new {@link RealAlgebraicElement}.
   *
   * @version 1.2.1
   * @since 1.0.0
   *
   * @example
   * import { RealAlgebra, ExactRealAlgebra, Polynomial as P }
   *   from '@kkitahara/real-algebra'
   * let ralg = new RealAlgebra()
   * let eralg = new ExactRealAlgebra()
   *
   * ralg.cast() // 0
   * ralg.cast(1) // 1
   * ralg.cast(true) // 1
   * ralg.cast(false) // 0
   *
   * let a = eralg.num(1)
   * let b = a
   * b === eralg.cast(a) // true
   * eralg.cast(1) instanceof P // true
   * b === eralg.cast(1) // false
   * eralg.eq(b, eralg.cast(1)) // true
   * eralg.eq(eralg.cast(), 0) // true
   */
  cast (a = 0) {
    return a + 0
  }

  /**
   * @desc
   * The RealAlgebra#copy method returns a copy of `a`.
   *
   * @param {RealAlgebraicElement} a
   * a {@link RealAlgebraicElement}.
   *
   * @return {RealAlgebraicElement}
   * a copy of `a`.
   *
   * @version 1.0.0
   * @since 1.0.0
   *
   * @example
   * import { RealAlgebra, ExactRealAlgebra } from '@kkitahara/real-algebra'
   * let ralg = new RealAlgebra()
   * let eralg = new ExactRealAlgebra()
   *
   * // copy does not work for numerical algebra
   * let a = ralg.num(1, 2)
   * ralg.copy(a) === a // true
   *
   * let b = eralg.num(1, 2)
   * let c = eralg.copy(b)
   * b === c // false
   * b.equals(c) // true
   */
  copy (a) {
    return a + 0
  }

  /**
   * @desc
   * The RealAlgebra#eq method checks if `a` is equal to `b`.
   * In the case of numerical algebra,
   * `a` is considered to be equal to `b` if the difference is smaller than
   * or equal to {@link RealAlgebra#eps}.
   *
   * @param {RealAlgebraicElement} a
   * a {@link RealAlgebraicElement}.
   *
   * @param {RealAlgebraicElement} b
   * a {@link RealAlgebraicElement}.
   *
   * @return {boolean}
   * `true` if `a` is equal to `b` and `false` otherwise.
   *
   * @version 1.0.0
   * @since 1.0.0
   *
   * @example
   * import { RealAlgebra, ExactRealAlgebra } from '@kkitahara/real-algebra'
   * let ralg = new RealAlgebra()
   * let ralg2 = new RealAlgebra(0.5)
   * let eralg = new ExactRealAlgebra()
   *
   * ralg.eq(ralg.num(1, 2), ralg.num(1, 2)) // true
   * ralg.eq(ralg.num(1, 2), ralg.num(2, 3)) // false
   *
   * // Equality can be controlled
   * ralg2.eq(ralg2.num(1, 2), ralg2.num(1, 2)) // true
   * ralg2.eq(ralg2.num(1, 2), ralg2.num(2, 3)) // true
   *
   * eralg.eq(eralg.num(1, 2), eralg.num(1, 2)) // true
   * eralg.eq(eralg.num(1, 2), eralg.num(2, 3)) // false
   */
  eq (a, b) {
    return Math.abs(a - b) <= this.eps
  }

  /**
   * @desc
   * The RealAlgebra#ne method checks if `a` is not equal to `b`.
   *
   * `ralg.ne(a, b)` is an alias for `!ralg.eq(a, b)`,
   * where `ralg` is an instance of {@link RealAlgebra}.
   *
   * @param {RealAlgebraicElement} a
   * a {@link RealAlgebraicElement}.
   *
   * @param {RealAlgebraicElement} b
   * a {@link RealAlgebraicElement}.
   *
   * @return {boolean}
   * `true` if `a` is not equal to `b` and `false` otherwise.
   *
   * @version 1.0.0
   * @since 1.0.0
   *
   * @example
   * import { RealAlgebra, ExactRealAlgebra } from '@kkitahara/real-algebra'
   * let ralg = new RealAlgebra()
   * let ralg2 = new RealAlgebra(0.5)
   * let eralg = new ExactRealAlgebra()
   *
   * ralg.ne(ralg.num(1, 2), ralg.num(1, 2)) // false
   * ralg.ne(ralg.num(1, 2), ralg.num(2, 3)) // true
   *
   * // Equality can be controlled
   * ralg2.ne(ralg2.num(1, 2), ralg2.num(1, 2)) // false
   * ralg2.ne(ralg2.num(1, 2), ralg2.num(2, 3)) // false
   *
   * eralg.ne(eralg.num(1, 2), eralg.num(1, 2)) // false
   * eralg.ne(eralg.num(1, 2), eralg.num(2, 3)) // true
   */
  ne (a, b) {
    return !this.eq(a, b)
  }

  /**
   * @desc
   * The RealAlgebra#isZero method checks if `a` is zero.
   * In the case of numerical algebra,
   * `a` is considered to be equal to zero if the absolute value of
   * `a` is smaller than or equal to {@link RealAlgebra#eps}.
   *
   * @param {RealAlgebraicElement} a
   * a {@link RealAlgebraicElement}.
   *
   * @return {boolean}
   * `true` if `a` is zero and `false` otherwise.
   *
   * @version 1.0.0
   * @since 1.0.0
   *
   * @example
   * import { RealAlgebra, ExactRealAlgebra } from '@kkitahara/real-algebra'
   * let ralg = new RealAlgebra()
   * let ralg2 = new RealAlgebra(0.5)
   * let eralg = new ExactRealAlgebra()
   *
   * ralg.isZero(ralg.num(-0, 2)) // true
   * ralg.isZero(ralg.num(0, 2)) // true
   * ralg.isZero(ralg.num(1, 2)) // false
   * ralg.isZero(ralg.num(-1, 2)) // false
   *
   * // Equality can be controlled
   * ralg2.isZero(ralg2.num(-0, 2)) // true
   * ralg2.isZero(ralg2.num(0, 2)) // true
   * ralg2.isZero(ralg2.num(1, 2)) // true
   * ralg2.isZero(ralg2.num(-1, 2)) // true
   *
   * eralg.isZero(eralg.num(0, 2)) // true
   * eralg.isZero(eralg.num(-0, 2)) // true
   * eralg.isZero(eralg.num(1, 2)) // false
   * eralg.isZero(eralg.num(-1, 2)) // false
   */
  isZero (a) {
    return Math.abs(a) <= this.eps
  }

  /**
   * @desc
   * The {@link RealAlgebra#isPositive} method checks
   * if `a` is positive (non-zero).
   * In the case of numerical algebra,
   * `a` is considered to be positive (non-zero) if
   * `a` is greater than {@link RealAlgebra#eps}.
   *
   * @param {RealAlgebraicElement} a
   * a {@link RealAlgebraicElement}.
   *
   * @return {boolean}
   * `true` if `a` is positive (non-zero) and `false` otherwise.
   *
   * @version 1.0.0
   * @since 1.0.0
   *
   * @example
   * import { RealAlgebra, ExactRealAlgebra } from '@kkitahara/real-algebra'
   * let ralg = new RealAlgebra()
   * let ralg2 = new RealAlgebra(0.5)
   * let eralg = new ExactRealAlgebra()
   *
   * ralg.isPositive(ralg.num(-0, 2)) // false
   * ralg.isPositive(ralg.num(0, 2)) // false
   * ralg.isPositive(ralg.num(1, 2)) // true
   * ralg.isPositive(ralg.num(-1, 2)) // false
   *
   * // Equality can be controlled
   * ralg2.isPositive(ralg2.num(-0, 2)) // false
   * ralg2.isPositive(ralg2.num(0, 2)) // false
   * ralg2.isPositive(ralg2.num(1, 2)) // false
   * ralg2.isPositive(ralg2.num(-1, 2)) // false
   *
   * eralg.isPositive(eralg.num(0, 2)) // false
   * eralg.isPositive(eralg.num(-0, 2)) // false
   * eralg.isPositive(eralg.num(1, 2)) // true
   * eralg.isPositive(eralg.num(-1, 2)) // false
   */
  isPositive (a) {
    return a > this.eps
  }

  /**
   * @desc
   * The RealAlgebra#isNegative method checks
   * if `a` is negative (non-zero).
   *
   * `ralg.isNegative(a)` is an alias for
   * `!(ralg.isZero(a) || ralg.isPositive(a))`,
   * where `ralg` is an instance of {@link RealAlgebra}.
   *
   * @param {RealAlgebraicElement} a
   * a {@link RealAlgebraicElement}.
   *
   * @return {boolean}
   * `true` if `a` is negative (non-zero) and `false` otherwise.
   *
   * @version 1.0.0
   * @since 1.0.0
   *
   * @example
   * import { RealAlgebra, ExactRealAlgebra } from '@kkitahara/real-algebra'
   * let ralg = new RealAlgebra()
   * let ralg2 = new RealAlgebra(0.5)
   * let eralg = new ExactRealAlgebra()
   *
   * ralg.isNegative(ralg.num(-0, 2)) // false
   * ralg.isNegative(ralg.num(0, 2)) // false
   * ralg.isNegative(ralg.num(1, 2)) // false
   * ralg.isNegative(ralg.num(-1, 2)) // true
   *
   * // Equality can be controlled
   * ralg2.isNegative(ralg2.num(-0, 2)) // false
   * ralg2.isNegative(ralg2.num(0, 2)) // false
   * ralg2.isNegative(ralg2.num(1, 2)) // false
   * ralg2.isNegative(ralg2.num(-1, 2)) // false
   *
   * eralg.isNegative(eralg.num(0, 2)) // false
   * eralg.isNegative(eralg.num(-0, 2)) // false
   * eralg.isNegative(eralg.num(1, 2)) // false
   * eralg.isNegative(eralg.num(-1, 2)) // true
   */
  isNegative (a) {
    return !(this.isZero(a) || this.isPositive(a))
  }

  /**
   * @desc
   * The RealAlgebra#isInteger method checks
   * if `a` is an integer.
   * `a` is considered to be an integer if
   * the difference between `a` and its nearest integer is
   * smaller than or equal to {@link RealAlgebra#eps}.
   *
   * @param {RealAlgebraicElement} a
   * a {@link RealAlgebraicElement}.
   *
   * @return {boolean}
   * `true` if `a` is considered to be an integer and `false` otherwise.
   *
   * @version 1.1.0
   * @since 1.1.0
   *
   * @example
   * import { RealAlgebra, ExactRealAlgebra } from '@kkitahara/real-algebra'
   * let ralg = new RealAlgebra()
   * let ralg2 = new RealAlgebra(0.5)
   * let eralg = new ExactRealAlgebra()
   *
   * ralg.isInteger(ralg.num(6, 3)) // true
   * ralg.isInteger(ralg.num(6, 3, 2)) // false
   * ralg.isInteger(ralg.num(-0, 2)) // true
   * ralg.isInteger(ralg.num(0, 2)) // true
   * ralg.isInteger(ralg.num(1, 3)) // false
   * ralg.isInteger(ralg.num(-1, 3)) // false
   * ralg.isInteger(ralg.num(1, 3, 0)) // true
   *
   * // Equality can be controlled
   * ralg2.isInteger(ralg2.num(6, 3)) // true
   * ralg2.isInteger(ralg2.num(6, 3, 2)) // true
   * ralg2.isInteger(ralg2.num(-0, 2)) // true
   * ralg2.isInteger(ralg2.num(0, 2)) // true
   * ralg2.isInteger(ralg2.num(1, 3)) // true
   * ralg2.isInteger(ralg2.num(-1, 3)) // true
   * ralg2.isInteger(ralg2.num(1, 3, 0)) // true
   *
   * eralg.isInteger(eralg.num(6, 3)) // true
   * eralg.isInteger(eralg.num(6, 3, 2)) // false
   * eralg.isInteger(eralg.num(0, 2)) // true
   * eralg.isInteger(eralg.num(-0, 2)) // true
   * eralg.isInteger(eralg.num(1, 3)) // false
   * eralg.isInteger(eralg.num(-1, 3)) // false
   * eralg.isInteger(eralg.num(1, 3, 0)) // true
   */
  isInteger (a) {
    return this.isZero(Math.abs(a - Math.round(a)))
  }

  /**
   * @desc
   * The RealAlgebra#isFinite method checks if `a` is finite.
   *
   * @param {RealAlgebraicElement} a
   * a {@link RealAlgebraicElement}.
   *
   * @return {boolean}
   * `true` if `a` is finite and `false` otherwise.
   *
   * @version 1.0.0
   * @since 1.0.0
   *
   * @example
   * import bigInt from 'big-integer'
   * import { RealAlgebra, ExactRealAlgebra } from '@kkitahara/real-algebra'
   * let ralg = new RealAlgebra()
   * let eralg = new ExactRealAlgebra()
   *
   * ralg.isFinite(ralg.num(1, 2)) // true
   * ralg.isFinite(ralg.num(1e999, 2)) // false
   *
   * eralg.isFinite(eralg.num(1, 2)) // true
   * eralg.isFinite(eralg.num(bigInt('1e999'), 2)) // true
   */
  isFinite (a) {
    return Number.isFinite(a + 0)
  }

  /**
   * @desc
   * The RealAlgebra#isExact method checks
   * if `this` is an implementation of exact algebra.
   *
   * @return {boolean}
   * `true` if `this` is an exact algebra and `false` otherwise.
   *
   * @version 1.0.0
   * @since 1.0.0
   *
   * @example
   * import { RealAlgebra, ExactRealAlgebra } from '@kkitahara/real-algebra'
   * let ralg = new RealAlgebra()
   * let eralg = new ExactRealAlgebra()
   *
   * ralg.isExact() // false
   * eralg.isExact() // true
   */
  isExact () {
    return false
  }

  /**
   * @desc
   * The RealAlgebra#isReal method checks
   * if `this` is an implementation of real algebra.
   *
   * @return {boolean}
   * `true`.
   *
   * @version 1.0.0
   * @since 1.0.0
   *
   * @example
   * import { RealAlgebra, ExactRealAlgebra } from '@kkitahara/real-algebra'
   * let ralg = new RealAlgebra()
   * let eralg = new ExactRealAlgebra()
   *
   * ralg.isReal() // true
   * eralg.isReal() // true
   */
  isReal () {
    return true
  }

  /**
   * @desc
   * The RealAlgebra#add method returns the result of the addition `a` plus `b`.
   *
   * `ralg.add(a, b)` is an alias for `ralg.iadd(ralg.copy(a), b)`,
   * where `ralg` is an instance of {@link RealAlgebra}.
   *
   * @param {RealAlgebraicElement} a
   * a {@link RealAlgebraicElement}.
   *
   * @param {RealAlgebraicElement} b
   * a {@link RealAlgebraicElement}.
   *
   * @return {RealAlgebraicElement}
   * a new {@link RealAlgebraicElement}
   * representing the result of the addition `a` plus `b`.
   *
   * @version 1.0.0
   * @since 1.0.0
   *
   * @example
   * import { RealAlgebra } from '@kkitahara/real-algebra'
   * let ralg = new RealAlgebra()
   *
   * let a = ralg.num(1, 2)
   * let b = ralg.num(1, 4)
   *
   * let c = ralg.add(a, b)
   * typeof c === 'number' // true
   * c !== a // true
   * ralg.eq(a, ralg.num(1, 2)) // true
   * ralg.eq(c, ralg.num(3, 4)) // true
   *
   * @example
   * import { ExactRealAlgebra as RealAlgebra, Polynomial as P }
   *   from '@kkitahara/real-algebra'
   * let ralg = new RealAlgebra()
   *
   * let a = ralg.num(1, 2)
   * let b = ralg.num(1, 4)
   *
   * let c = ralg.add(a, b)
   * typeof c === 'number' // false
   * c instanceof P // true
   * c !== a // true
   * ralg.eq(a, ralg.num(1, 2)) // true
   * ralg.eq(c, ralg.num(3, 4)) // true
   */
  add (a, b) {
    return this.iadd(this.copy(a), b)
  }

  /**
   * @desc
   * The RealAlgebra#iadd method adds `b` to `a` *in place*.
   *
   * Since the in-place operation does not work for built-in {@link number}s,
   * use this method like `a = ralg.iadd(a, b)`,
   * where `ralg` is an instance of {@link RealAlgebra},
   * if the result should be stored in `a`.
   *
   * @param {RealAlgebraicElement} a
   * a {@link RealAlgebraicElement}.
   *
   * @param {RealAlgebraicElement} b
   * a {@link RealAlgebraicElement}.
   *
   * @return {RealAlgebraicElement}
   * `a`, or a new {@link RealAlgebraicElement} if `a` is not a valid
   * {@link RealAlgebraicElement}.
   *
   * @version 1.0.0
   * @since 1.0.0
   *
   * @example
   * import { RealAlgebra } from '@kkitahara/real-algebra'
   * let ralg = new RealAlgebra()
   *
   * let a = ralg.num(1, 2)
   * let b = ralg.num(1, 4)
   *
   * // In-place operation does not work for built-in numbers
   * // ANTI-PATTERN!
   * ralg.iadd(a, b)
   * typeof a === 'number' // true
   * ralg.eq(a, ralg.num(3, 4)) // false
   *
   * @example
   * import { RealAlgebra } from '@kkitahara/real-algebra'
   * let ralg = new RealAlgebra()
   *
   * let a = ralg.num(1, 2)
   * let b = ralg.num(1, 4)
   *
   * // GOOD-PRACTICE!
   * a = ralg.iadd(a, b)
   * typeof a === 'number' // true
   * ralg.eq(a, ralg.num(3, 4)) // true
   *
   * @example
   * import { ExactRealAlgebra as RealAlgebra, Polynomial as P }
   *   from '@kkitahara/real-algebra'
   * let ralg = new RealAlgebra()
   *
   * let a = ralg.num(1, 2)
   * let b = ralg.num(1, 4)
   * let c = a
   *
   * // GOOD-PRACTICE!
   * a = ralg.iadd(a, b)
   * a === c // true
   * typeof a === 'number' // false
   * a instanceof P // true
   * ralg.eq(a, ralg.num(3, 4)) // true
   * ralg.eq(c, ralg.num(3, 4)) // true
   */
  iadd (a, b) {
    a += b
    return a
  }

  /**
   * @desc
   * The RealAlgebra#sub method returns the result of
   * the subtraction `a` minus `b`.
   *
   * `ralg.sub(a, b)` is an alias for `ralg.isub(ralg.copy(a), b)`,
   * where `ralg` is an instance of {@link RealAlgebra}.
   *
   * @param {RealAlgebraicElement} a
   * a {@link RealAlgebraicElement}.
   *
   * @param {RealAlgebraicElement} b
   * a {@link RealAlgebraicElement}.
   *
   * @return {RealAlgebraicElement}
   * a new {@link RealAlgebraicElement}
   * representing the result of the subtraction `a` minus `b`.
   *
   * @version 1.0.0
   * @since 1.0.0
   *
   * @example
   * import { RealAlgebra } from '@kkitahara/real-algebra'
   * let ralg = new RealAlgebra()
   *
   * let a = ralg.num(1, 2)
   * let b = ralg.num(1, 4)
   *
   * let c = ralg.sub(a, b)
   * typeof c === 'number' // true
   * c !== a // true
   * ralg.eq(a, ralg.num(1, 2)) // true
   * ralg.eq(c, ralg.num(1, 4)) // true
   *
   * @example
   * import { ExactRealAlgebra as RealAlgebra, Polynomial as P }
   *   from '@kkitahara/real-algebra'
   * let ralg = new RealAlgebra()
   *
   * let a = ralg.num(1, 2)
   * let b = ralg.num(1, 4)
   *
   * let c = ralg.sub(a, b)
   * typeof c === 'number' // false
   * c instanceof P // true
   * c !== a // true
   * ralg.eq(a, ralg.num(1, 2)) // true
   * ralg.eq(c, ralg.num(1, 4)) // true
   */
  sub (a, b) {
    return this.isub(this.copy(a), b)
  }

  /**
   * @desc
   * The RealAlgebra#isub method subtracts `b` from `a` *in place*.
   *
   * Since the in-place operation does not work for built-in {@link number}s,
   * use this method like `a = ralg.isub(a, b)`,
   * where `ralg` is an instance of {@link RealAlgebra},
   * if the result should be stored in `a`.
   *
   * @param {RealAlgebraicElement} a
   * a {@link RealAlgebraicElement}.
   *
   * @param {RealAlgebraicElement} b
   * a {@link RealAlgebraicElement}.
   *
   * @return {RealAlgebraicElement}
   * `a`, or new {@link RealAlgebraicElement} if `a` is not a valid
   * {@link RealAlgebraicElement}.
   *
   * @version 1.0.0
   * @since 1.0.0
   *
   * @example
   * import { RealAlgebra } from '@kkitahara/real-algebra'
   * let ralg = new RealAlgebra()
   *
   * let a = ralg.num(1, 2)
   * let b = ralg.num(1, 4)
   *
   * // In-place operation does not work for built-in numbers
   * // ANTI-PATTERN!
   * ralg.isub(a, b)
   * typeof a === 'number' // true
   * ralg.eq(a, ralg.num(1, 4)) // false
   *
   * @example
   * import { RealAlgebra } from '@kkitahara/real-algebra'
   * let ralg = new RealAlgebra()
   *
   * let a = ralg.num(1, 2)
   * let b = ralg.num(1, 4)
   *
   * // GOOD-PRACTICE!
   * a = ralg.isub(a, b)
   * typeof a === 'number' // true
   * ralg.eq(a, ralg.num(1, 4)) // true
   *
   * @example
   * import { ExactRealAlgebra as RealAlgebra, Polynomial as P }
   *   from '@kkitahara/real-algebra'
   * let ralg = new RealAlgebra()
   *
   * let a = ralg.num(1, 2)
   * let b = ralg.num(1, 4)
   * let c = a
   *
   * // GOOD-PRACTICE!
   * a = ralg.isub(a, b)
   * a === c // true
   * typeof a === 'number' // false
   * a instanceof P // true
   * ralg.eq(a, ralg.num(1, 4)) // true
   * ralg.eq(c, ralg.num(1, 4)) // true
   */
  isub (a, b) {
    a -= b
    return a
  }

  /**
   * @desc
   * The RealAlgebra#mul method returns the result of
   * the multiplication `a` times `b`.
   *
   * `ralg.mul(a, b)` is an alias for `ralg.imul(ralg.copy(a), b)`,
   * where `ralg` is an instance of {@link RealAlgebra}.
   *
   * @param {RealAlgebraicElement} a
   * a {@link RealAlgebraicElement}.
   *
   * @param {RealAlgebraicElement} b
   * a {@link RealAlgebraicElement}.
   *
   * @return {RealAlgebraicElement}
   * a new {@link RealAlgebraicElement}
   * representing the result of the multiplication `a` times `b`.
   *
   * @version 1.0.0
   * @since 1.0.0
   *
   * @example
   * import { RealAlgebra } from '@kkitahara/real-algebra'
   * let ralg = new RealAlgebra()
   *
   * let a = ralg.num(1, 2)
   * let b = ralg.num(1, 4)
   *
   * let c = ralg.mul(a, b)
   * typeof c === 'number' // true
   * c !== a // true
   * ralg.eq(a, ralg.num(1, 2)) // true
   * ralg.eq(c, ralg.num(1, 8)) // true
   *
   * @example
   * import { ExactRealAlgebra as RealAlgebra, Polynomial as P }
   *   from '@kkitahara/real-algebra'
   * let ralg = new RealAlgebra()
   *
   * let a = ralg.num(1, 2)
   * let b = ralg.num(1, 4)
   *
   * let c = ralg.mul(a, b)
   * typeof c === 'number' // false
   * c instanceof P // true
   * c !== a // true
   * ralg.eq(a, ralg.num(1, 2)) // true
   * ralg.eq(c, ralg.num(1, 8)) // true
   */
  mul (a, b) {
    return this.imul(this.copy(a), b)
  }

  /**
   * @desc
   * The RealAlgebra#imul method multiplies `a` by `b` *in place*.
   *
   * Since the in-place operation does not work for built-in {@link number}s,
   * use this method like `a = ralg.imul(a, b)`,
   * where `ralg` is an instance of {@link RealAlgebra},
   * if the result should be stored in `a`.
   *
   * @param {RealAlgebraicElement} a
   * a {@link RealAlgebraicElement}.
   *
   * @param {RealAlgebraicElement} b
   * a {@link RealAlgebraicElement}.
   *
   * @return {RealAlgebraicElement}
   * `a`, or a new {@link RealAlgebraicElement} if `a` is not a valid
   * {@link RealAlgebraicElement}.
   *
   * @version 1.0.0
   * @since 1.0.0
   *
   * @example
   * import { RealAlgebra } from '@kkitahara/real-algebra'
   * let ralg = new RealAlgebra()
   *
   * let a = ralg.num(1, 2)
   * let b = ralg.num(1, 4)
   *
   * // In-place operation does not work for built-in numbers
   * // ANTI-PATTERN!
   * ralg.imul(a, b)
   * typeof a === 'number' // true
   * ralg.eq(a, ralg.num(1, 8)) // false
   *
   * @example
   * import { RealAlgebra } from '@kkitahara/real-algebra'
   * let ralg = new RealAlgebra()
   *
   * let a = ralg.num(1, 2)
   * let b = ralg.num(1, 4)
   *
   * // GOOD-PRACTICE!
   * a = ralg.imul(a, b)
   * typeof a === 'number' // true
   * ralg.eq(a, ralg.num(1, 8)) // true
   *
   * @example
   * import { ExactRealAlgebra as RealAlgebra, Polynomial as P }
   *   from '@kkitahara/real-algebra'
   * let ralg = new RealAlgebra()
   *
   * let a = ralg.num(1, 2)
   * let b = ralg.num(1, 4)
   * let c = a
   *
   * // GOOD-PRACTICE!
   * a = ralg.imul(a, b)
   * a === c // true
   * typeof a === 'number' // false
   * a instanceof P // true
   * ralg.eq(a, ralg.num(1, 8)) // true
   * ralg.eq(c, ralg.num(1, 8)) // true
   */
  imul (a, b) {
    a *= b
    return a
  }

  /**
   * @desc
   * The RealAlgebra#div method returns the result of the division `a` over `b`.
   *
   * `ralg.div(a, b)` is an alias for `ralg.idiv(ralg.copy(a), b)`,
   * where `ralg` is an instance of {@link RealAlgebra}.
   *
   * @param {RealAlgebraicElement} a
   * a {@link RealAlgebraicElement}.
   *
   * @param {RealAlgebraicElement} b
   * a {@link RealAlgebraicElement}.
   *
   * @return {RealAlgebraicElement}
   * a new {@link RealAlgebraicElement}
   * representing the result of the division `a` over `b`.
   *
   * @version 1.0.0
   * @since 1.0.0
   *
   * @example
   * import { RealAlgebra } from '@kkitahara/real-algebra'
   * let ralg = new RealAlgebra()
   *
   * let a = ralg.num(1, 2)
   * let b = ralg.num(1, 4)
   *
   * let c = ralg.div(a, b)
   * typeof c === 'number' // true
   * c !== a // true
   * ralg.eq(a, ralg.num(1, 2)) // true
   * ralg.eq(c, ralg.num(2)) // true
   *
   * @example
   * import { ExactRealAlgebra as RealAlgebra, Polynomial as P }
   *   from '@kkitahara/real-algebra'
   * let ralg = new RealAlgebra()
   *
   * let a = ralg.num(1, 2)
   * let b = ralg.num(1, 4)
   *
   * let c = ralg.div(a, b)
   * typeof c === 'number' // false
   * c instanceof P // true
   * c !== a // true
   * ralg.eq(a, ralg.num(1, 2)) // true
   * ralg.eq(c, ralg.num(2)) // true
   */
  div (a, b) {
    return this.idiv(this.copy(a), b)
  }

  /**
   * @desc
   * The RealAlgebra#idiv method divides `a` by `b` *in place*.
   *
   * Since the in-place operation does not work for built-in {@link number}s,
   * use this method like `a = ralg.idiv(a, b)`,
   * where `ralg` is an instance of {@link RealAlgebra},
   * if the result should be stored in `a`.
   *
   * @param {RealAlgebraicElement} a
   * a {@link RealAlgebraicElement}.
   *
   * @param {RealAlgebraicElement} b
   * a {@link RealAlgebraicElement}.
   *
   * @return {RealAlgebraicElement}
   * `a`, or a new {@link RealAlgebraicElement} if `a` is not a valid
   * {@link RealAlgebraicElement}.
   *
   * @version 1.0.0
   * @since 1.0.0
   *
   * @example
   * import { RealAlgebra } from '@kkitahara/real-algebra'
   * let ralg = new RealAlgebra()
   *
   * let a = ralg.num(1, 2)
   * let b = ralg.num(1, 4)
   *
   * // In-place operation does not work for built-in numbers
   * // ANTI-PATTERN!
   * ralg.idiv(a, b)
   * typeof a === 'number' // true
   * ralg.eq(a, ralg.num(2)) // false
   *
   * @example
   * import { RealAlgebra } from '@kkitahara/real-algebra'
   * let ralg = new RealAlgebra()
   *
   * let a = ralg.num(1, 2)
   * let b = ralg.num(1, 4)
   *
   * // GOOD-PRACTICE!
   * a = ralg.idiv(a, b)
   * typeof a === 'number' // true
   * ralg.eq(a, ralg.num(2)) // true
   *
   * @example
   * import { ExactRealAlgebra as RealAlgebra, Polynomial as P }
   *   from '@kkitahara/real-algebra'
   * let ralg = new RealAlgebra()
   *
   * let a = ralg.num(1, 2)
   * let b = ralg.num(1, 4)
   * let c = a
   *
   * // GOOD-PRACTICE!
   * a = ralg.idiv(a, b)
   * a === c // true
   * typeof a === 'number' // false
   * a instanceof P // true
   * ralg.eq(a, ralg.num(2)) // true
   * ralg.eq(c, ralg.num(2)) // true
   */
  idiv (a, b) {
    a /= b
    return a
  }

  /**
   * @desc
   * The RealAlgebra#neg method returns the result of
   * the multiplication `a` times `-1`.
   *
   * `ralg.neg(a)` is an alias for `ralg.ineg(ralg.copy(a))`,
   * where `ralg` is an instance of {@link RealAlgebra}.
   *
   * @param {RealAlgebraicElement} a
   * a {@link RealAlgebraicElement}.
   *
   * @return {RealAlgebraicElement}
   * a new {@link RealAlgebraicElement}
   * representing the result of the multiplication `a` times `-1`.
   *
   * @version 1.0.0
   * @since 1.0.0
   *
   * @example
   * import { RealAlgebra } from '@kkitahara/real-algebra'
   * let ralg = new RealAlgebra()
   *
   * let a = ralg.num(1, 2)
   *
   * let b = ralg.neg(a)
   * typeof b === 'number' // true
   * b !== a // true
   * ralg.eq(a, ralg.num(1, 2)) // true
   * ralg.eq(b, ralg.num(-1, 2)) // true
   *
   * @example
   * import { ExactRealAlgebra as RealAlgebra, Polynomial as P }
   *   from '@kkitahara/real-algebra'
   * let ralg = new RealAlgebra()
   *
   * let a = ralg.num(1, 2)
   *
   * let b = ralg.neg(a)
   * typeof b === 'number' // false
   * b instanceof P // true
   * b !== a // true
   * ralg.eq(a, ralg.num(1, 2)) // true
   * ralg.eq(b, ralg.num(-1, 2)) // true
   */
  neg (a) {
    return this.ineg(this.copy(a))
  }

  /**
   * @desc
   * The RealAlgebra#ineg method multiplies `a` by `-1` *in place*.
   *
   * Since the in-place operation does not work for built-in {@link number}s,
   * use this method like `a = ralg.ineg(a)`,
   * where `ralg` is an instance of {@link RealAlgebra},
   * if the result should be stored in `a`.
   *
   * @param {RealAlgebraicElement} a
   * a {@link RealAlgebraicElement}.
   *
   * @return {RealAlgebraicElement}
   * `a`, or a new {@link RealAlgebraicElement} if `a` is not a valid
   * {@link RealAlgebraicElement}.
   *
   * @version 1.0.0
   * @since 1.0.0
   *
   * @example
   * import { RealAlgebra } from '@kkitahara/real-algebra'
   * let ralg = new RealAlgebra()
   *
   * let a = ralg.num(1, 2)
   *
   * // In-place operation does not work for built-in numbers
   * // ANTI-PATTERN!
   * ralg.ineg(a)
   * typeof a === 'number' // true
   * ralg.eq(a, ralg.num(-1, 2)) // false
   *
   * @example
   * import { RealAlgebra } from '@kkitahara/real-algebra'
   * let ralg = new RealAlgebra()
   *
   * let a = ralg.num(1, 2)
   *
   * // GOOD-PRACTICE!
   * a = ralg.ineg(a)
   * typeof a === 'number' // true
   * ralg.eq(a, ralg.num(-1, 2)) // true
   *
   * @example
   * import { ExactRealAlgebra as RealAlgebra, Polynomial as P }
   *   from '@kkitahara/real-algebra'
   * let ralg = new RealAlgebra()
   *
   * let a = ralg.num(1, 2)
   * let b = a
   *
   * // GOOD-PRACTICE!
   * a = ralg.ineg(a)
   * a === b // true
   * typeof a === 'number' // false
   * a instanceof P // true
   * ralg.eq(a, ralg.num(-1, 2)) // true
   * ralg.eq(b, ralg.num(-1, 2)) // true
   */
  ineg (a) {
    a = -a
    return a
  }

  /**
   * @desc
   * The RealAlgebra#cjg method returns the complex conjugate of `a`.
   *
   * `ralg.cjg(a)` is an alias for `ralg.icjg(ralg.copy(a))`,
   * where `ralg` is an instance of {@link RealAlgebra}.
   *
   * @param {RealAlgebraicElement} a
   * a {@link RealAlgebraicElement}.
   *
   * @return {RealAlgebraicElement}
   * a new {@link RealAlgebraicElement}
   * representing the complex conjugate of `a`.
   *
   * @version 1.0.0
   * @since 1.0.0
   *
   * @example
   * import { RealAlgebra } from '@kkitahara/real-algebra'
   * let ralg = new RealAlgebra()
   *
   * let a = ralg.num(1, 2)
   *
   * let b = ralg.cjg(a)
   * typeof b === 'number' // true
   * b !== a // false
   * ralg.eq(a, ralg.num(1, 2)) // true
   * ralg.eq(b, ralg.num(1, 2)) // true
   *
   * @example
   * import { ExactRealAlgebra as RealAlgebra, Polynomial as P }
   *   from '@kkitahara/real-algebra'
   * let ralg = new RealAlgebra()
   *
   * let a = ralg.num(1, 2)
   *
   * let b = ralg.cjg(a)
   * typeof b === 'number' // false
   * b instanceof P // true
   * b !== a // true
   * ralg.eq(a, ralg.num(1, 2)) // true
   * ralg.eq(b, ralg.num(1, 2)) // true
   */
  cjg (a) {
    return this.icjg(this.copy(a))
  }

  /**
   * @desc
   * The RealAlgebra#icjg method
   * evaluates the complex conjugate of `a` and stores the result to `a`.
   *
   * Since the in-place operation does not work for built-in {@link number}s,
   * use this method like `a = ralg.icjg(a)`,
   * where `ralg` is an instance of {@link RealAlgebra},
   * if the result should be stored in `a`.
   *
   * @param {RealAlgebraicElement} a
   * a {@link RealAlgebraicElement}.
   *
   * @return {RealAlgebraicElement}
   * `a`, or a new {@link RealAlgebraicElement} if `a` is not a valid
   * {@link RealAlgebraicElement}.
   *
   * @version 1.0.0
   * @since 1.0.0
   *
   * @example
   * import { RealAlgebra } from '@kkitahara/real-algebra'
   * let ralg = new RealAlgebra()
   *
   * let a = ralg.num(1, 2)
   *
   * // In-place operation does not work for built-in numbers
   * // ANTI-PATTERN!
   * ralg.icjg(a)
   * typeof a === 'number' // true
   * ralg.eq(a, ralg.num(1, 2)) // true
   *
   * @example
   * import { RealAlgebra } from '@kkitahara/real-algebra'
   * let ralg = new RealAlgebra()
   *
   * let a = ralg.num(1, 2)
   *
   * // GOOD-PRACTICE!
   * a = ralg.icjg(a)
   * typeof a === 'number' // true
   * ralg.eq(a, ralg.num(1, 2)) // true
   *
   * @example
   * import { ExactRealAlgebra as RealAlgebra, Polynomial as P }
   *   from '@kkitahara/real-algebra'
   * let ralg = new RealAlgebra()
   *
   * let a = ralg.num(1, 2)
   * let b = a
   *
   * // GOOD-PRACTICE!
   * a = ralg.icjg(a)
   * a === b // true
   * typeof a === 'number' // false
   * a instanceof P // true
   * ralg.eq(a, ralg.num(1, 2)) // true
   * ralg.eq(b, ralg.num(1, 2)) // true
   */
  icjg (a) {
    return a + 0
  }

  /**
   * @desc
   * The RealAlgebra#abs method returns the absolute value of `a`.
   *
   * `ralg.abs(a)` is an alias for `ralg.iabs(ralg.copy(a))`,
   * where `ralg` is an instance of {@link RealAlgebra}.
   *
   * @param {RealAlgebraicElement} a
   * a {@link RealAlgebraicElement}.
   *
   * @return {RealAlgebraicElement}
   * a new {@link RealAlgebraicElement}
   * representing the absolute value of `a`.
   *
   * @version 1.0.0
   * @since 1.0.0
   *
   * @example
   * import { RealAlgebra } from '@kkitahara/real-algebra'
   * let ralg = new RealAlgebra()
   *
   * let a = ralg.num(1, 2)
   * let b = ralg.num(-1, 2)
   *
   * let c = ralg.abs(a)
   * let d = ralg.abs(b)
   * typeof c === 'number' // true
   * typeof d === 'number' // true
   * c !== a // false
   * d !== b // true
   * ralg.eq(a, ralg.num(1, 2)) // true
   * ralg.eq(c, ralg.num(1, 2)) // true
   * ralg.eq(b, ralg.num(-1, 2)) // true
   * ralg.eq(d, ralg.num(1, 2)) // true
   *
   * @example
   * import { ExactRealAlgebra as RealAlgebra, Polynomial as P }
   *   from '@kkitahara/real-algebra'
   * let ralg = new RealAlgebra()
   *
   * let a = ralg.num(1, 2)
   * let b = ralg.num(-1, 2)
   *
   * let c = ralg.abs(a)
   * let d = ralg.abs(b)
   * typeof c === 'number' // false
   * typeof d === 'number' // false
   * c instanceof P // true
   * d instanceof P // true
   * c !== a // true
   * d !== b // true
   * ralg.eq(a, ralg.num(1, 2)) // true
   * ralg.eq(c, ralg.num(1, 2)) // true
   * ralg.eq(b, ralg.num(-1, 2)) // true
   * ralg.eq(d, ralg.num(1, 2)) // true
   */
  abs (a) {
    return this.iabs(this.copy(a))
  }

  /**
   * @desc
   * The RealAlgebra#iabs method multiplies `a` by `-1` *in place*
   * if `a` is negative.
   *
   * Since the in-place operation does not work for built-in {@link number}s,
   * use this method like `a = ralg.iabs(a)`,
   * where `ralg` is an instance of {@link RealAlgebra},
   * if the result should be stored in `a`.
   *
   * @param {RealAlgebraicElement} a
   * a {@link RealAlgebraicElement}.
   *
   * @return {RealAlgebraicElement}
   * `a`, or a new {@link RealAlgebraicElement} if `a` is not a valid
   * {@link RealAlgebraicElement}.
   *
   * @version 1.0.0
   * @since 1.0.0
   *
   * @example
   * import { RealAlgebra } from '@kkitahara/real-algebra'
   * let ralg = new RealAlgebra()
   *
   * let a = ralg.num(1, 2)
   * let b = ralg.num(-1, 2)
   *
   * // In-place operation does not work for built-in numbers
   * // ANTI-PATTERN!
   * ralg.iabs(a)
   * ralg.iabs(b)
   * typeof a === 'number' // true
   * typeof b === 'number' // true
   * ralg.eq(a, ralg.num(1, 2)) // true
   * ralg.eq(b, ralg.num(1, 2)) // false
   *
   * @example
   * import { RealAlgebra } from '@kkitahara/real-algebra'
   * let ralg = new RealAlgebra()
   *
   * let a = ralg.num(1, 2)
   * let b = ralg.num(-1, 2)
   *
   * // GOOD-PRACTICE!
   * a = ralg.iabs(a)
   * b = ralg.iabs(b)
   * typeof a === 'number' // true
   * typeof b === 'number' // true
   * ralg.eq(a, ralg.num(1, 2)) // true
   * ralg.eq(b, ralg.num(1, 2)) // true
   *
   * @example
   * import { ExactRealAlgebra as RealAlgebra, Polynomial as P }
   *   from '@kkitahara/real-algebra'
   * let ralg = new RealAlgebra()
   *
   * let a = ralg.num(1, 2)
   * let b = ralg.num(-1, 2)
   * let c = a
   * let d = b
   *
   * // GOOD-PRACTICE!
   * a = ralg.iabs(a)
   * b = ralg.iabs(b)
   * a === c // true
   * b === d // true
   * typeof a === 'number' // false
   * typeof b === 'number' // false
   * a instanceof P // true
   * b instanceof P // true
   * ralg.eq(a, ralg.num(1, 2)) // true
   * ralg.eq(b, ralg.num(1, 2)) // true
   */
  iabs (a) {
    a = Math.abs(a)
    return a
  }

  /**
   * @desc
   * The RealAlgebra#abs2 method returns
   * the square of the absolute value of `a`.
   *
   * `ralg.abs2(a)` is an alias for `ralg.mul(a, a)`,
   * where `ralg` is an instance of {@link RealAlgebra}.
   *
   * @param {RealAlgebraicElement} a
   * a {@link RealAlgebraicElement}.
   *
   * @return {RealAlgebraicElement}
   * a new {@link RealAlgebraicElement}
   * representing the square of the absolute value of `a`.
   *
   * @version 1.0.0
   * @since 1.0.0
   *
   * @example
   * import { RealAlgebra } from '@kkitahara/real-algebra'
   * let ralg = new RealAlgebra()
   *
   * let a = ralg.num(-1, 2)
   *
   * let b = ralg.abs2(a)
   * typeof b === 'number' // true
   * b !== a // true
   * ralg.eq(a, ralg.num(-1, 2)) // true
   * ralg.eq(b, ralg.num(1, 4)) // true
   *
   * @example
   * import { ExactRealAlgebra as RealAlgebra, Polynomial as P }
   *   from '@kkitahara/real-algebra'
   * let ralg = new RealAlgebra()
   *
   * let a = ralg.num(-1, 2)
   *
   * let b = ralg.abs2(a)
   * typeof b === 'number' // false
   * b instanceof P // true
   * b !== a // true
   * ralg.eq(a, ralg.num(-1, 2)) // true
   * ralg.eq(b, ralg.num(1, 4)) // true
   */
  abs2 (a) {
    return this.mul(a, a)
  }

  /**
   * @desc
   * The RealAlgebra#iabs2 method multiplies `a` by `a` *in place*.
   *
   * Since the in-place operation does not work for built-in {@link number}s,
   * use this method like `a = ralg.iabs2(a)`,
   * where `ralg` is an instance of {@link RealAlgebra},
   * if the result should be stored in `a`.
   *
   * `ralg.iabs2(a)` is an alias for `ralg.imul(a, a)`.
   *
   * @param {RealAlgebraicElement} a
   * a {@link RealAlgebraicElement}.
   *
   * @return {RealAlgebraicElement}
   * `a`, or a new {@link RealAlgebraicElement} if `a` is not a valid
   * {@link RealAlgebraicElement}.
   *
   * @version 1.0.0
   * @since 1.0.0
   *
   * @example
   * import { RealAlgebra } from '@kkitahara/real-algebra'
   * let ralg = new RealAlgebra()
   *
   * let a = ralg.num(-1, 2)
   *
   * // In-place operation does not work for built-in numbers
   * // ANTI-PATTERN!
   * ralg.iabs2(a)
   * typeof a === 'number' // true
   * ralg.eq(a, ralg.num(1, 4)) // false
   *
   * @example
   * import { RealAlgebra } from '@kkitahara/real-algebra'
   * let ralg = new RealAlgebra()
   *
   * let a = ralg.num(-1, 2)
   *
   * // GOOD-PRACTICE!
   * a = ralg.iabs2(a)
   * typeof a === 'number' // true
   * ralg.eq(a, ralg.num(1, 4)) // true
   *
   * @example
   * import { ExactRealAlgebra as RealAlgebra, Polynomial as P }
   *   from '@kkitahara/real-algebra'
   * let ralg = new RealAlgebra()
   *
   * let a = ralg.num(-1, 2)
   * let b = a
   *
   * // GOOD-PRACTICE!
   * a = ralg.iabs2(a)
   * a === b // true
   * typeof a === 'number' // false
   * a instanceof P // true
   * ralg.eq(a, ralg.num(1, 4)) // true
   * ralg.eq(b, ralg.num(1, 4)) // true
   */
  iabs2 (a) {
    a = this.imul(a, a)
    return a
  }

  /**
   * @desc
   * The reviver function for the {@link RealAlgebraicElement}s.
   *
   * @param {object} key
   *
   * @param {object} value
   *
   * @return {RealAlgebraicElement|Object}
   * a {@link RealAlgebraicElement} or `value`.
   *
   * @version 1.0.0
   * @since 1.0.0
   *
   * @example
   * import { RealAlgebra } from '@kkitahara/real-algebra'
   * let ralg = new RealAlgebra()
   *
   * let a = ralg.num(1, 2, 3)
   * let s = JSON.stringify(a)
   * let b = JSON.parse(s, ralg.reviver)
   *
   * typeof s // 'string'
   * ralg.eq(a, b) // true
   * a === b // true
   *
   * @example
   * import { ExactRealAlgebra as RealAlgebra } from '@kkitahara/real-algebra'
   * let ralg = new RealAlgebra()
   *
   * let a = ralg.num(1, 2, 3)
   * let s = JSON.stringify(a)
   * let b = JSON.parse(s, ralg.reviver)
   *
   * typeof s // 'string'
   * ralg.eq(a, b) // true
   * a === b // false
   */
  reviver (key, value) {
    return value
  }
}

/* @license-end */
