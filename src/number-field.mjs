/**
 * @source: https://www.npmjs.com/package/@kkitahara/real-algebra
 * @license magnet:?xt=urn:btih:8e4f440f4c65981c5bf93c76d35135ba5064d8b7&dn=apache-2.0.txt Apache-2.0
 */

import { bigInt } from './big-integer.mjs'
import { bigRat } from './big-rational.mjs'

/**
 * Copyright 2019 Koichi Kitahara
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/**
 * @desc
 * The Monomial class represents the mathematical expression
 *
 * (*p* / *q*)sqrt(*b*),
 *
 * where *p* is an integer, *q* is a positive (non-zero) integer,
 * and *b* is a positive, square-free integer.
 *
 * @version 1.0.0
 * @since 1.0.0
 */
export class Monomial {
  /**
   * @desc
   * The constructor function of the the {@link Monomial} class.
   *
   * Parameters `numCoef`, `denomCoef` and `sqBasis` correspond to
   * *p*, *q* and *b*, respectively, in the expression (*p* / *q*)sqrt(*b*).
   *
   * If the value of the expression is zero, then *p* = 0, *q* = 1 and
   * *b* = 1 are set.
   *
   * CAUTION: this constructor function does not check if `sqBasis` is
   * square-free.
   *
   * @param {number|bigInt} [numCoef = bigInt.zero]
   * a number.
   *
   * @param {number|bigInt} [denomCoef = bigInt.one]
   * a non-zero integer (can be negative here).
   *
   * @param {number|bigInt} [sqBasis = bigInt.one]
   * a positive, square-free integer.
   *
   * @throws {Error}
   * if a parameter is neither {@link number} nor {@link bigInt}.
   *
   * @throws {Error}
   * if `denomCoef` or `sqBasis` is not an integer.
   *
   * @throws {Error}
   * if `numCoef` is zero.
   *
   * @throws {Error}
   * if `sqBasis` is negative.
   *
   * @version 1.2.0
   * @since 1.0.0
   *
   * @todo support for built-in `BigInt` object.
   *
   * @example
   * import bigInt from 'big-integer'
   * import bigRat from 'big-rational'
   * import { Monomial as M } from '@kkitahara/real-algebra'
   *
   * let a = new M(1, -2, 5)
   * a instanceof M // true
   * a.coef.equals(bigRat(1, -2)) // true
   * a.sqBasis.equals(5) // true
   *
   * // parameters can be `bigInt`
   * a.equals(new M(bigInt(1), bigInt(-2), bigInt(5))) // true
   *
   * new M(1, -2).equals(new M(1, -2, 1)) // true
   * new M(5).equals(new M(5, 1, 1)) // true
   * new M().equals(new M(0, 1, 1)) // true
   *
   * let b = new M(1, -2, 0)
   * b instanceof M // true
   * b.coef.equals(bigRat(1, -2)) // false
   * b.sqBasis.equals(0) // false
   * // if `b` is zero, `coef` is zero and `sqBasis` is one
   * b.coef.equals(0) // true
   * b.sqBasis.equals(1) // true
   *
   * let c = new M(0, -2, 5)
   * c instanceof M // true
   * c.coef.equals(0) // true
   * c.sqBasis.equals(5) // false
   * // if `coef` is zero, then `sqBasis` is one
   * c.sqBasis.equals(1) // true
   *
   * let d = new M(1.1, -2, 1)
   * d instanceof M // true
   * d.coef.equals(bigRat(-11, 20)) // true
   *
   * // Invalid parameter type
   * new M(null, -2, 1) // Error
   * new M(1, null, 1) // Error
   * new M(1, -2, null) // Error
   *
   * // Non-integer parameter
   * new M(1, -2.1, 1) // Error
   * new M(1, -2, 1.1) // Error
   *
   * // `numCoef` is zero
   * new M(1, 0, -1) // Error
   *
   * // `sqBasis` is negative
   * new M(1, -2, -1) // Error
   *
   * // `sqBasis` must be square-free, but the constructor does not check it.
   * // ANTI-PATTERN!
   * new M(1, -2, 4) instanceof M // true
   */
  constructor (
    numCoef = bigInt.zero,
    denomCoef = bigInt.one,
    sqBasis = bigInt.one) {
    if (typeof denomCoef === 'number') {
      if (Number.isInteger(denomCoef)) {
        denomCoef = bigInt(denomCoef)
      } else {
        throw Error('`denomCoef` must be an integer.')
      }
    } else if (!bigInt.isInstance(denomCoef)) {
      throw Error('`denomCoef` must be a `number` or `bigInt`.')
    }
    if (denomCoef.isZero()) {
      throw Error('`denomCoef` must no be zero.')
    }
    if (typeof numCoef === 'number' || bigInt.isInstance(numCoef)) {
      numCoef = bigRat(numCoef)
    } else {
      throw Error('`numCoef` must be a `number` or `bigInt`.')
    }
    if (typeof sqBasis === 'number') {
      if (Number.isInteger(sqBasis)) {
        sqBasis = bigInt(sqBasis)
      } else {
        throw Error('`sqBasis` must be an integer.')
      }
    } else if (!bigInt.isInstance(sqBasis)) {
      throw Error('`sqBasis` must be a `number` or `bigInt`.')
    }
    let coef
    if (numCoef.isZero() || sqBasis.isZero()) {
      coef = bigRat.zero
      sqBasis = bigInt.one
    } else if (sqBasis.isPositive()) {
      coef = numCoef.over(denomCoef)
    } else {
      throw Error('`sqBasis` must not be negative.')
    }
    /**
     * @desc
     * Monomial#coef (coeficient) represents *p* / *q*
     * in the expression (*p* / *q*)sqrt(*b*).
     *
     * @type {bigRat}
     *
     * @version 1.0.0
     * @since 1.0.0
     */
    this.coef = coef
    /**
     * @desc
     * Monomial#sqBasis (square of the basis) represents *b*
     * in the expression (*p* / *q*)sqrt(*b*).
     *
     * @type {bigInt}
     *
     * @version 1.0.0
     * @since 1.0.0
     */
    this.sqBasis = sqBasis
  }

  /**
   * @desc
   * The Monomial#copy method returns a copy of `this`.
   *
   * @return {Monomial}
   * a copy of `this`.
   *
   * @version 1.0.0
   * @since 1.0.0
   *
   * @example
   * import { Monomial as M } from '@kkitahara/real-algebra'
   *
   * let a = new M(1, -2, 5)
   *
   * let b = a
   * b instanceof M // true
   * b === a // true
   * b.equals(a) // true
   *
   * let c = a.copy()
   * c instanceof M // true
   * c === b // false
   * c.equals(a) // true
   */
  copy () {
    return new this.constructor(this.coef.num, this.coef.denom, this.sqBasis)
  }

  /**
   * @desc
   * The Monomial#compareBasis method compares
   * `this.sqBasis` to `another.sqBasis`.
   *
   * @param {Monomial} another
   * an instance of {@link Monomial}.
   *
   * @return {number}
   * `-1` if `this.sqBasis` is less than `another.sqBasis`.
   *
   * `0` if `this.sqBasis` is equal to `another.sqBasis`.
   *
   * `1` if `this.sqBasis` is greater than `another.sqBasis`.
   *
   * @throws {Error}
   * if `another` is not an instance of {@link Monomial}.
   *
   * @version 1.0.0
   * @since 1.0.0
   *
   * @example
   * import { Monomial as M } from '@kkitahara/real-algebra'
   *
   * let a = new M(1, -2, 2)
   * let b = new M(2, -3, 3)
   * let c = new M(1, -2, 3)
   *
   * // a.sqBasis < b.sqBasis
   * a.compareBasis(b) // -1
   *
   * // c.sqBasis == b.sqBasis
   * c.compareBasis(b) // 0
   *
   * // b.sqBasis > a.sqBasis
   * b.compareBasis(a) // 1
   *
   * a.compareBasis(null) // Error
   */
  compareBasis (another) {
    if (!(another instanceof Monomial)) {
      throw Error('`another` is not an instance of `Monomial`.')
    }
    return this.sqBasis.compare(another.sqBasis)
  }

  /**
   * @desc
   * The Monomial#hasSameBasis method checks
   * if `this.sqBasis` is equal to `another.sqBasis`.
   *
   * @param {Monomial} another
   * an instance of {@link Monomial}.
   *
   * @return {boolean}
   * `true` if `this.sqBasis` is equal to `another.sqBasis`
   * and `false` otherwise.
   *
   * @throws {Error}
   * if `another` is not an instance of {@link Monomial}.
   *
   * @version 1.0.0
   * @since 1.0.0
   *
   * @example
   * import { Monomial as M } from '@kkitahara/real-algebra'
   *
   * let a = new M(1, -2, 3)
   *
   * a.hasSameBasis(new M(2, -3, 3)) // true
   * a.hasSameBasis(new M(2, -3, 4)) // false
   * a.hasSameBasis(null) // Error
   */
  hasSameBasis (another) {
    if (!(another instanceof Monomial)) {
      throw Error('`another` is not an instance of `Monomial`.')
    }
    return this.sqBasis.equals(another.sqBasis)
  }

  /**
   * @desc
   * The Monomial#equals method checks if `this` is equal to `another`.
   *
   * @param {Monomial} another
   * an instance of {@link Monomial}.
   *
   * @return {boolean}
   * `true` if `this` is equal to `another` and `false` otherwise.
   *
   * @throws {Error}
   * if `another` is not an instance of {@link Monomial}.
   *
   * @version 1.0.0
   * @since 1.0.0
   *
   * @example
   * import { Monomial as M } from '@kkitahara/real-algebra'
   *
   * let a = new M(1, -2, 3)
   *
   * a.equals(new M(-1, 2, 3)) // true
   * a.equals(new M(2, -3, 3)) // false
   * a.equals(new M(1, -2, 2)) // false
   * a.equals(null) // Error
   */
  equals (another) {
    if (!(another instanceof Monomial)) {
      throw Error('`another` is not an instance of `Monomial`.')
    }
    return this.hasSameBasis(another) && this.coef.equals(another.coef)
  }

  /**
   * @desc
   * The Monomial#isZero method checks if `this` is zero.
   *
   * @return {boolean}
   * `true` if `this` is zero and `false` otherwise.
   *
   * @version 1.0.0
   * @since 1.0.0
   *
   * @example
   * import { Monomial as M } from '@kkitahara/real-algebra'
   *
   * new M(0, -2, 3).isZero() // true
   * new M(1, -2, 0).isZero() // true
   * new M(1, -2, 3).isZero() // false
   */
  isZero () {
    return this.coef.isZero()
  }

  /**
   * @desc
   * The Monomial#isPositive method checks if `this` is positive (non-zero).
   *
   * @return {boolean}
   * `true` if `this` is positive (non-zero)
   * and `false` otherwise (negative or zero).
   *
   * @version 1.0.0
   * @since 1.0.0
   *
   * @example
   * import { Monomial as M } from '@kkitahara/real-algebra'
   *
   * new M(1, 2, 3).isPositive() // true
   * new M(0, 2, 3).isPositive() // false
   * new M(1, 2, 0).isPositive() // false
   * new M(1, -2, 3).isPositive() // false
   */
  isPositive () {
    return this.coef.isPositive()
  }

  /**
   * @desc
   * The Monomial#isInteger method checks if `this` is an integer.
   *
   * @return {boolean}
   * `true` if `this` is an integer and `false` otherwise.
   *
   * @version 1.1.0
   * @since 1.1.0
   *
   * @example
   * import { Monomial as M } from '@kkitahara/real-algebra'
   *
   * new M(6, 3, 1).isInteger() // true
   * new M(0, 2, 3).isInteger() // true
   * new M(1, 2, 0).isInteger() // true
   * new M(1, 2, 1).isInteger() // false
   * new M(2, 1, 1).isInteger() // true
   * new M(2, 1, 3).isInteger() // false
   */
  isInteger () {
    return this.coef.denom.isUnit() && this.sqBasis.equals(bigInt.one)
  }

  /**
   * @desc
   * The Monomial#iaddToCoef method adds `addend` to `this.coef` *in place*.
   *
   * CAUTION: this method does not check the type of `addend`.
   *
   * @param {number|bigInt|bigRat} addend
   * an integer or a rational.
   *
   * @return {Monomial}
   * `this`.
   *
   * @version 1.0.0
   * @since 1.0.0
   *
   * @example
   * import bigRat from 'big-rational'
   * import { Monomial as M } from '@kkitahara/real-algebra'
   *
   * let a = new M(1, 2, 3)
   * let b = a.iaddToCoef(bigRat(1, 3))
   *
   * b === a // true
   * a.equals(new M(5, 6, 3)) // true
   * b.equals(new M(5, 6, 3)) // true
   */
  iaddToCoef (addend) {
    this.coef = this.coef.plus(addend)
    return this
  }

  /**
   * @desc
   * The Monomial#isubFromCoef method subtracts `subtrahend`
   * from `this.coef` *in place*.
   *
   * CAUTION: this method does not check the type of `subtrahend`.
   *
   * @param {number|bigInt|bigRat} subtrahend
   * an integer or a rational.
   *
   * @return {Monomial}
   * `this`.
   *
   * @version 1.0.0
   * @since 1.0.0
   *
   * @example
   * import bigRat from 'big-rational'
   * import { Monomial as M } from '@kkitahara/real-algebra'
   *
   * let a = new M(1, 2, 3)
   * let b = a.isubFromCoef(bigRat(1, 3))
   *
   * b === a // true
   * a.equals(new M(1, 6, 3)) // true
   * b.equals(new M(1, 6, 3)) // true
   */
  isubFromCoef (subtrahend) {
    this.coef = this.coef.minus(subtrahend)
    return this
  }

  /**
   * @desc
   * The Monomial#imul method multiplies `this` by `another` *in place*.
   *
   * @param {Monomial} another
   * an instance of {@link Monomial}.
   *
   * @return {Monomial}
   * `this`.
   *
   * @throws {Error}
   * if `another` is not an instance of {@link Monomial}.
   *
   * @version 1.0.0
   * @since 1.0.0
   *
   * @example
   * import { Monomial as M } from '@kkitahara/real-algebra'
   *
   * let a = new M(1, 2, 2)
   * let b = a.imul(new M(2, 3, 3))
   *
   * b === a // true
   * a.equals(new M(1, 3, 6)) // true
   * b.equals(new M(1, 3, 6)) // true
   *
   * @example
   * import { Monomial as M } from '@kkitahara/real-algebra'
   *
   * let a = new M(1, 2, 3)
   * let b = a.imul(new M(2, 3, 3))
   *
   * b.equals(new M(1, 1, 1)) // true
   *
   * a.imul(null) // Error
   */
  imul (another) {
    if (!(another instanceof Monomial)) {
      throw Error('`another` is not an instance of `Monomial`.')
    }
    const gcd = bigInt.gcd(this.sqBasis, another.sqBasis)
    this.coef = this.coef.times(another.coef).times(gcd)
    this.sqBasis = this.sqBasis.over(gcd).times(another.sqBasis.over(gcd))
    return this
  }

  /**
   * @desc
   * The Monomial#idiv method divides `this` by `another` *in place*.
   *
   * @param {Monomial} another
   * an instance of {@link Monomial}.
   *
   * @return {Monomial}
   * `this`.
   *
   * @throws {Error}
   * if `another` is not an instance of {@link Monomial}.
   *
   * @throws {Error}
   * if `another` is zero.
   *
   * @version 1.0.0
   * @since 1.0.0
   *
   * @example
   * import { Monomial as M } from '@kkitahara/real-algebra'
   *
   * let a = new M(1, 2, 2)
   * let b = a.idiv(new M(2, 3, 3))
   *
   * b === a // true
   * a.equals(new M(1, 4, 6)) // true
   * b.equals(new M(1, 4, 6)) // true
   *
   * @example
   * import { Monomial as M } from '@kkitahara/real-algebra'
   *
   * let a = new M(1, 2, 3)
   * let b = a.idiv(new M(2, 3, 3))
   *
   * b.equals(new M(3, 4, 1)) // true
   *
   * a.idiv(null) // Error
   * a.idiv(new M(0, 3, 3)) // Error
   */
  idiv (another) {
    if (!(another instanceof Monomial)) {
      throw Error('`another` is not an instance of `Monomial`.')
    } else if (another.isZero()) {
      throw Error('`another` is zero.')
    }
    const gcd = bigInt.gcd(this.sqBasis, another.sqBasis)
    this.coef = this.coef.over(another.coef).times(gcd).over(another.sqBasis)
    this.sqBasis = this.sqBasis.over(gcd).times(another.sqBasis.over(gcd))
    return this
  }

  /**
   * @desc
   * The Monomial#ineg method multiplies `this` by `-1` *in place*.
   *
   * @return {Monomial}
   * `this`.
   *
   * @version 1.0.0
   * @since 1.0.0
   *
   * @example
   * import { Monomial as M } from '@kkitahara/real-algebra'
   *
   * let a = new M(1, 2, 2)
   * let b = a.ineg()
   *
   * b === a // true
   * a.equals(new M(-1, 2, 2)) // true
   * b.equals(new M(-1, 2, 2)) // true
   */
  ineg () {
    this.coef = this.coef.negate()
    return this
  }

  /**
   * @desc
   * The Monomial#valueOf method converts `this` to a built-in {@link number}.
   *
   * @return {number}
   * the numerical value of `this`.
   *
   * @version 1.0.0
   * @since 1.0.0
   *
   * @example
   * import { Monomial as M } from '@kkitahara/real-algebra'
   *
   * new M(1, 1, 2).valueOf() // Math.sqrt(2)
   *
   * @example
   * import { Monomial as M } from '@kkitahara/real-algebra'
   *
   * let tau = (1 + Math.sqrt(5)) / 2
   *
   * 0.5 + new M(1, 2, 5) // tau
   */
  valueOf () {
    return this.coef.valueOf() * Math.sqrt(this.sqBasis.valueOf())
  }

  /**
   * @desc
   * The Monomial#toString method converts
   * `this` to a human-readable {@link string}.
   *
   * Let `123` be the string representation of `this`,
   * it returns `123` if `isTrainling` is a falsy value
   * and ` + 123` otherwise.
   *
   * Let `-123` be the string representation of `this`,
   * it returns `-123` if `isTrainling` is a falsy value
   * and ` - 123` otherwise.
   *
   * @param {number} [radix]
   * the base to use for representing numeric values.
   *
   * @param {boolean} [isTrailing = false]
   * whether it is trailing another {@list Monomial} or not.
   *
   * @return {string}
   * a human-readable {@link string} representation of `this`.
   *
   * @version 1.1.0
   * @since 1.0.0
   *
   * @example
   * import { Monomial as M } from '@kkitahara/real-algebra'
   *
   * new M(1, 2, 5).toString() // '(1 / 2)sqrt(5)'
   * new M(1, 2, 5).toString(2) // '(1 / 10)sqrt(101)'
   * new M(1, 2, 5).toString(10, true) // ' + (1 / 2)sqrt(5)'
   * new M(-1, 2, 5).toString() // '-(1 / 2)sqrt(5)'
   * new M(-1, 1, 5).toString(10) // '-sqrt(5)'
   * new M(-2, 1, 5).toString(10) // '-2sqrt(5)'
   */
  toString (radix, isTrailing = false) {
    let s = ''
    if (isTrailing) {
      if (this.coef.isNegative()) {
        s += ' - '
      } else {
        s += ' + '
      }
    } else if (this.coef.isNegative()) {
      s += '-'
    }
    if (this.coef.denom.isUnit()) {
      if (this.coef.num.isUnit()) {
        if (this.sqBasis.equals(bigInt.one)) {
          s += bigInt.one.toString(radix)
        } else {
          s += 'sqrt(' + this.sqBasis.toString(radix) + ')'
        }
      } else {
        s += this.coef.num.abs().toString(radix)
        if (!this.sqBasis.equals(bigInt.one)) {
          s += 'sqrt(' + this.sqBasis.toString(radix) + ')'
        }
      }
    } else {
      if (this.sqBasis.equals(bigInt.one)) {
        s += this.coef.num.abs().toString(radix) + ' / ' +
            this.coef.denom.abs().toString(radix)
      } else {
        s += '(' + this.coef.num.abs().toString(radix) + ' / ' +
                 this.coef.denom.abs().toString(radix) + ')'
        s += 'sqrt(' + this.sqBasis.toString(radix) + ')'
      }
    }
    return s
  }
}

/**
 * @desc
 * The Polynomial class represents the sum of {@link Monomial}s.
 * It is implemented as a subclass of {@link Array}.
 *
 * @version 1.0.0
 * @since 1.0.0
 */
export class Polynomial extends Array {
  /**
   * @desc
   * The constructor function of the {@link Polynomial} class.
   *
   * It internally uses {@link Array} constructor,
   * but only instances of {@link Monomial} should be given as parameters.
   *
   * {@link Polynomial#sort} and {@link Polynomial#simplify}
   * are called before returning.
   *
   * @param {...Monomial} args
   * instances of {@link Monomial}.
   *
   * @throws {Error}
   * if a parameter is not an instance of {@link Monomial}.
   *
   * @version 1.0.0
   * @since 1.0.0
   *
   * @example
   * import { Monomial as M, Polynomial as P } from '@kkitahara/real-algebra'
   *
   * let a = new P()
   *
   * a instanceof P // true
   * a.length === 0 // true
   * a.isZero() // true
   *
   * let b = new P(new M(1, 2))
   *
   * b instanceof P // true
   * b.length === 1 // true
   *
   * let c = new P(new M(1, 2), new M(1, 2, 5))
   *
   * c instanceof P // true
   * c.length === 2 // true
   *
   * // It throws an error
   * new P(1) // Error
   */
  constructor (...args) {
    for (let i = args.length - 1; i >= 0; i -= 1) {
      if (!(args[i] instanceof Monomial)) {
        throw Error('args[' + i + '] is not an instance of `Monomial`.')
      }
    }
    super(...args)
    this.sort()
    this.simplify()
  }

  /**
   * @desc
   * Thanks to this species, built-in {@link Array} methods,
   * such as the {@link Polynomial}#slice method,
   * return an {@link Array} object.
   *
   * @type {function}
   *
   * @version 1.0.0
   * @since 1.0.0
   *
   * @example
   * import { Monomial as M, Polynomial as P } from '@kkitahara/real-algebra'
   *
   * let a = new P(new M(1, 2), new M(1, 2, 5))
   * let b = a.slice()
   *
   * a.length === 2 // true
   * b.length === 2 // true
   * a === b // false
   * a.constructor === P // true
   * b.constructor === P // false
   * b.constructor === Array // true
   */
  static get [Symbol.species] () {
    return Array
  }

  /**
   * @desc
   * The Polynomial.zero function returns
   * an instance of {@link Polynomial} representing zero (an empty array).
   *
   * @return {Polynomial}
   * an instance of {@link Polynomial} representing zero.
   *
   * @version 1.0.0
   * @since 1.0.0
   *
   * @example
   * import { Polynomial as P } from '@kkitahara/real-algebra'
   *
   * let a = P.zero()
   *
   * a instanceof P // true
   * a.length === 0 // true
   * a.isZero() // true
   */
  static zero () {
    return new Polynomial()
  }

  /**
   * @desc
   * The Polynomial#copy method returns a copy of `this`.
   *
   * @return {Polynomial}
   * a copy of `this`.
   *
   * @version 1.0.0
   * @since 1.0.0
   *
   * @example
   * import { Monomial as M, Polynomial as P } from '@kkitahara/real-algebra'
   *
   * let a = new P(new M(1, 2), new M(1, 2, 5))
   * let b = a.copy()
   *
   * b instanceof P // true
   * b.equals(a) // true
   * b !== a // true
   */
  copy () {
    const another = new this.constructor()
    for (let i = 0, n = this.length; i < n; i += 1) {
      another.push(this[i].copy())
    }
    return another
  }

  /**
   * @desc
   * The Polynomial#sort method sorts the contents of `this` *in place*.
   * A {@link Monominal} of smaller {@link Monomial#sqBasis} precedes.
   *
   * @return {Polynomial}
   * `this`.
   *
   * @version 1.0.0
   * @since 1.0.0
   *
   * @example
   * import { Monomial as M, Polynomial as P } from '@kkitahara/real-algebra'
   *
   * let a = new P(new M(1, 2, 5))
   * a.push(new M(1, 2))
   *
   * a[0].compareBasis(a[1]) // 1
   *
   * a.sort()
   *
   * a[0].compareBasis(a[1]) // -1
   *
   * // Automatically sorted by the constructor
   * let b = new P(new M(1, 2, 5), new M(1, 2))
   *
   * b[0].compareBasis(b[1]) // -1
   */
  sort () {
    return super.sort((a, b) => a.compareBasis(b))
  }

  /**
   * @desc
   * The Polynomial#simplify method simplifies
   * the contents of `this` *in place* as follows.
   * * {@link Monomial}s of the same {@link Monomial#sqBasis} are merged.
   * * {@link Monomial}s of zero {@link Monomial#coef} are removed.
   *
   * CAUTION: this method assumes the contents of `this` are sorted.
   * To sort the contents, use {@link Polynomial#sort}.
   *
   * @return {Polynomial}
   * `this`.
   *
   * @version 1.0.0
   * @since 1.0.0
   *
   * @example
   * import { Monomial as M, Polynomial as P } from '@kkitahara/real-algebra'
   *
   * // Monomials of the same basis are merged
   * let a = new P(new M(1, 4))
   * a.push(new M(1, 2, 5))
   * a.push(new M(1, 4))
   * a.sort().simplify()
   *
   * a.length === 2 // true
   * a.equals(new P(new M(1, 2), new M(1, 2, 5))) // true
   *
   * // A Polynomial must be sorted before the simplification
   * // ANTI-PATTERN!
   * let b = new P(new M(1, 4))
   * b.push(new M(1, 2, 5))
   * b.push(new M(1, 4))
   * b.simplify()
   *
   * // Not merged
   * b.length === 2 // false
   * b.length === 3 // true
   * b.equals(new P(new M(1, 2), new M(1, 2, 5))) // false
   *
   * // Monomials of zero coefficient are removed
   * let c = new P(new M(1, 4))
   * c.push(new M(0, 2, 5))
   * c.push(new M(1, 4))
   * c.sort().simplify()
   *
   * c.length === 1 // true
   * c.equals(new P(new M(1, 2))) // true
   *
   * // Automatically sorted and simplified by the constructor
   * let d = new P(new M(1, 4), new M(0, 2, 5), new M(1, 4))
   *
   * d.length === 1 // true
   * d.equals(new P(new M(1, 2))) // true
   */
  simplify () {
    while (this.length > 0 && this[this.length - 1].isZero()) {
      this.pop()
    }
    for (let i = this.length - 2; i >= 0; i -= 1) {
      if (this[i].isZero()) {
        this.splice(i, 1)
      } else if (this[i].hasSameBasis(this[i + 1])) {
        this[i].iaddToCoef(this[i + 1].coef)
        this.splice(i + 1, 1)
        if (this[i].isZero()) {
          this.splice(i, 1)
          i = i - 1
        }
      }
    }
    return this
  }

  /**
   * @desc
   * The Polynomial#equals method checks if `this` is equal to `another`.
   *
   * CAUTION: this method does not check if `another` is
   * an instance of {@link Polynomial}.
   *
   * @param {Polynomial} another
   * an instance of {@link Polynomial}.
   *
   * @return {boolean}
   * `true` if `this` is equal to `another` and `false` otherwise.
   *
   * @throws {Error}
   * if `another` is not an instance of {@link Polynomial}.
   *
   * @version 1.0.0
   * @since 1.0.0
   *
   * @example
   * import { Monomial as M, Polynomial as P } from '@kkitahara/real-algebra'
   *
   * let a = new P(new M(1, 2), new M(1, 2, 5))
   *
   * a.equals(new P(new M(1, 2), new M(1, 2, 5))) // true
   * a.equals(new P(new M(1, 2), new M(1, 2, 3))) // false
   * a.equals(null) // Error
   */
  equals (another) {
    if (!(another instanceof Polynomial)) {
      throw Error('`another` is not an instance of `Polynomial`.')
    }
    return this.length === another.length && this.every((element, index) => {
      return element.equals(another[index])
    })
  }

  /**
   * @desc
   * The Polynomial#isZero method checks if `this` is zero.
   *
   * @return {boolean}
   * `true` if `this` is zero and `false` otherwise.
   *
   * @version 1.0.0
   * @since 1.0.0
   *
   * @example
   * import { Monomial as M, Polynomial as P } from '@kkitahara/real-algebra'
   *
   * new P(new M(0, 2, 3), new M(0, 2, 5)).isZero() // true
   * new P(new M(0, 2, 3), new M(1, 2, 5)).isZero() // false
   * new P().isZero() // true
   */
  isZero () {
    return this.length === 0
  }

  /**
   * @desc
   * The Polynomial#isPositive method checks if `this` is positive (non-zero).
   *
   * @return {boolean}
   * `true` if `this` is positive (non-zero)
   * and `false` otherwise (negative or zero).
   *
   * @version 1.2.0
   * @since 1.0.0
   *
   * @example
   * import { Monomial as M, Polynomial as P } from '@kkitahara/real-algebra'
   *
   * new P(new M(1, 2, 5)).isPositive() // true
   * new P(new M(-1, 2, 5)).isPositive() // false
   * new P().isPositive() // false
   * new P(new M(-1, 2, 3), new M(1, 2, 5)).isPositive() // true
   * new P(new M(1, 2, 3), new M(-1, 2, 5)).isPositive() // false
   * new P(new M(1, 2, 3), new M(-1, 2, 5), new M(1, 2, 1)).isPositive() // true
   * new P(new M(1, 1, 6), new M(-1, 1, 10)).isPositive() // false
   */
  isPositive () {
    if (this.isZero()) { // this.length === 0
      return false
    } else if (this.length === 1) {
      return this[0].isPositive()
    } else {
      const isPositive0 = this[0].isPositive() ? 1 : 0
      if (this.slice(1).every((element) => {
        return (isPositive0 ^ (element.isPositive() ? 1 : 0)) === 0
      })) {
        return isPositive0 === 1
      } else {
        const n = this.length - 1
        let sqFac = this[n].sqBasis
        const temp1 = new Polynomial()
        const temp2 = new Polynomial()
        const temp3 = new Polynomial()
        for (let i = 0; i < n; i += 1) {
          const gcd = bigInt.gcd(sqFac, this[i].sqBasis)
          if (gcd.notEquals(bigInt.one)) {
            sqFac = gcd
            temp2.push(this[i].copy())
            temp3.push(this[i].copy().ineg())
          } else {
            temp1.push(this[i].copy())
            temp3.push(this[i].copy())
          }
        }
        temp2.push(this[n].copy())
        temp3.push(this[n].copy().ineg())
        if (temp3.imul(this).isPositive()) {
          return temp1.isPositive()
        } else {
          const fac = new Polynomial(new Monomial(1, 1, sqFac))
          return temp2.imul(fac).isPositive()
        }
      }
    }
  }

  /**
   * @desc
   * The Polynomial#compare method compares `this` to `another`.
   *
   * CAUTION: this method does not check if `another` is
   * an instance of {@link Polynomial}.
   *
   * @param {Polynomial} another
   * an instance of {@link Polynomial}.
   *
   * @return {number}
   * `-1` if `this` is less than `another`.
   *
   * `0` if `this` is equal to `another`.
   *
   * `1` if `this` is greater than `another`.
   *
   * @throws {Error}
   * if `another` is not an instance of {@link Polynomial}.
   *
   * @version 1.0.0
   * @since 1.0.0
   *
   * @example
   * import { Monomial as M, Polynomial as P } from '@kkitahara/real-algebra'
   *
   * let a = new P(new M(-1, 2, 3), new M(1, 2, 5))
   * let b = new P(new M(1, 2, 3), new M(-1, 2, 5))
   * let c = new P(new M(-1, 2, 3), new M(1, 2, 5))
   *
   * a.compare(b) // 1
   * b.compare(a) // -1
   * a.compare(c) // 0
   * a.compare(null) // Error
   */
  compare (another) {
    if (!(another instanceof Polynomial)) {
      throw Error('`another` is not an instance of `Polynomial`.')
    }
    const tmp = this.copy().isub(another)
    if (tmp.isZero()) {
      return 0
    } else if (tmp.isPositive()) {
      return 1
    } else {
      return -1
    }
  }

  /**
   * @desc
   * The Polymial#isInteger method checks if `this` is an integer.
   *
   * @return {boolean}
   * `true` if `this` is an integer and `false` otherwise.
   *
   * @version 1.1.0
   * @since 1.1.0
   *
   * @example
   * import { Monomial as M, Polynomial as P } from '@kkitahara/real-algebra'
   *
   * new P().isInteger() // true
   * new P(new M(1, 2, 5)).isInteger() // false
   * new P(new M(1, 2, 0)).isInteger() // true
   * new P(new M(0, 2, 2)).isInteger() // true
   * new P(new M(6, 3, 1)).isInteger() // true
   * new P(new M(6, 3, 5), new M(6, -3, 5)).isInteger() // true
   * new P(new M(1, 1, 1), new M(6, -3, 5)).isInteger() // false
   * new P(new M(6, -3, 5), new M(6, 3, 1)).isInteger() // false
   * new P(new M(1, 4, 1), new M(3, 4, 1)).isInteger() // true
   */
  isInteger () {
    return this.isZero() || (this.length === 1 && this[0].isInteger())
  }

  /**
   * @desc
   * The Polynomial#iadd method adds `another` to `this` *in place*.
   *
   * CAUTION: this method assumes the contents of `this`
   * and `another` are sorted.
   * To sort the contents, use {@link Polynomial#sort}.
   *
   * @param {Polynomial} another
   * an instance of {@link Polynomial}.
   *
   * @return {Polynomial}
   * `this`.
   *
   * @throws {Error}
   * if `another` is not an instance of {@link Polynomial}.
   *
   * @version 1.0.5
   * @since 1.0.0
   *
   * @example
   * import { Monomial as M, Polynomial as P } from '@kkitahara/real-algebra'
   *
   * let a = new P(new M(-1, 2, 3), new M(1, 2, 5))
   * let b = new P(new M(1, 2, 3), new M(-1, 2, 5))
   * a.iadd(b)
   *
   * b.equals(P.zero()) // false
   * a.equals(P.zero()) // true
   *
   * a = new P(new M(-1, 2, 3), new M(1, 2, 5))
   * a[0].compareBasis = () => 2
   * a.iadd(b) // Error
   *
   * @example
   * import { Monomial as M, Polynomial as P } from '@kkitahara/real-algebra'
   *
   * let a = new P(new M(-1, 2, 3), new M(1, 2, 5))
   * let b = a
   * let c = new P(new M(-1, 1, 3), new M(1, 1, 5))
   * a.iadd(a)
   *
   * a === b // true
   * a.equals(c) // true
   * b.equals(c) // true
   *
   * a.iadd(null) // Error
   */
  iadd (another) {
    if (!(another instanceof Polynomial)) {
      throw Error('`another` is not an instance of `Polynomial`.')
    }
    another = another.slice()
    const n = another.length
    let j = 0
    for (let i = 0, m = this.length; i < m && j < n;) {
      switch (this[i].compareBasis(another[j])) {
        case (-1):
          i += 1
          break
        case (0):
          this[i].iaddToCoef(another[j].coef)
          if (this[i].isZero()) {
            this.splice(i, 1)
            m -= 1
          } else {
            i += 1
          }
          j += 1
          break
        case (1):
          this.splice(i, 0, another[j].copy())
          m += 1
          i += 1
          j += 1
          break
        default:
          throw Error('unexpected value.')
      }
    }
    for (; j < n; j += 1) {
      this.push(another[j].copy())
    }
    return this
  }

  /**
   * @desc
   * The Polynomial#isub method subtracts `another` from `this` *in place*.
   *
   * CAUTION: this method assumes the contents of `this`
   * and `another` are sorted.
   * To sort the contents, use {@link Polynomial#sort}.
   *
   * @param {Polynomial} another
   * an instance of {@link Polynomial}.
   *
   * @return {Polynomial}
   * `this`.
   *
   * @throws {Error}
   * if `another` is not an instance of {@link Polynomial}.
   *
   * @version 1.0.5
   * @since 1.0.0
   *
   * @example
   * import { Monomial as M, Polynomial as P } from '@kkitahara/real-algebra'
   *
   * let a = new P(new M(-1, 2, 3), new M(1, 2, 5))
   * let b = new P(new M(-1, 2, 3), new M(1, 2, 5))
   * a.isub(b)
   *
   * b.equals(P.zero()) // false
   * a.equals(P.zero()) // true
   *
   * a = new P(new M(-1, 2, 3), new M(1, 2, 5))
   * a[0].compareBasis = () => 2
   * a.isub(b) // Error
   *
   * @example
   * import { Monomial as M, Polynomial as P } from '@kkitahara/real-algebra'
   *
   * let a = new P(new M(-1, 2, 3), new M(1, 2, 5))
   * let b = a
   * a.isub(a)
   *
   * a === b // true
   * a.equals(P.zero()) // true
   * b.equals(P.zero()) // true
   *
   * a.isub(null) // Error
   *
   * @example
   * import { Monomial as M, Polynomial as P } from '@kkitahara/real-algebra'
   *
   * let a = new P(new M(1, 2, 3))
   * let b = new P(new M(1, 2, 5))
   * a.isub(b)
   *
   * a.equals(new P(new M(1, 2, 3), new M(-1, 2, 5))) // true
   *
   * @example
   * import { Monomial as M, Polynomial as P } from '@kkitahara/real-algebra'
   *
   * let a = new P(new M(1, 2, 3))
   * let b = new P(new M(1, 2, 5))
   * a.isub(b)
   *
   * a.equals(new P(new M(1, 2, 3), new M(-1, 2, 5))) // true
   */
  isub (another) {
    if (!(another instanceof Polynomial)) {
      throw Error('`another` is not an instance of `Polynomial`.')
    }
    another = another.slice()
    const n = another.length
    let j = 0
    for (let i = 0, m = this.length; i < m && j < n;) {
      switch (this[i].compareBasis(another[j])) {
        case (-1):
          i += 1
          break
        case (0):
          this[i].isubFromCoef(another[j].coef)
          if (this[i].isZero()) {
            this.splice(i, 1)
            m -= 1
          } else {
            i += 1
          }
          j += 1
          break
        case (1):
          this.splice(i, 0, another[j].copy().ineg())
          m += 1
          i += 1
          j += 1
          break
        default:
          throw Error('unexpected value.')
      }
    }
    for (; j < n; j += 1) {
      this.push(another[j].copy().ineg())
    }
    return this
  }

  /**
   * @desc
   * The Polynomial#imul method multiplies `this` by `another` *in place*.
   *
   * {@link Polynomial#sort} and {@link Polynomial#simplify}
   * are called before returning.
   *
   * @param {Polynomial} another
   * an instance of {@link Polynomial}.
   *
   * @return {Polynomial}
   * `this`.
   *
   * @throws {Error}
   * if `another` is not an instance of {@link Polynomial}.
   *
   * @version 1.0.0
   * @since 1.0.0
   *
   * @example
   * import { Monomial as M, Polynomial as P } from '@kkitahara/real-algebra'
   *
   * let a = new P(new M(1, 2), new M(1, 2, 5))
   * let b = new P(new M(-1, 2), new M(1, 2, 5))
   * a.imul(b)
   *
   * b.equals(new P(new M(1))) // false
   * a.equals(new P(new M(1))) // true
   *
   * @example
   * import { Monomial as M, Polynomial as P } from '@kkitahara/real-algebra'
   *
   * let a = new P(new M(1, 2), new M(1, 2, 5))
   * let b = a
   * let c = new P(new M(3, 2), new M(1, 2, 5))
   * a.imul(a)
   *
   * a === b // true
   * a.equals(c) // true
   * b.equals(c) // true
   *
   * a.imul(null) // Error
   */
  imul (another) {
    if (!(another instanceof Polynomial)) {
      throw Error('`another` is not an instance of `Polynomial`.')
    }
    const swap = this.slice()
    another = another.slice()
    this.splice(0)
    for (let i = swap.length - 1; i >= 0; i -= 1) {
      for (let j = another.length - 1; j >= 0; j -= 1) {
        this.push(swap[i].copy().imul(another[j]))
      }
    }
    this.sort()
    this.simplify()
    return this
  }

  /**
   * @desc
   * The Polynomial#idiv method divides `this` by `another` *in place*.
   *
   * {@link Polynomial#sort} and {@link Polynomial#simplify}
   * are called before returning.
   *
   * @param {Polynomial} another
   * an instance of {@link Polynomial}.
   *
   * @return {Polynomial}
   * `this`.
   *
   * @throws {Error}
   * if `another` is not an instance of {@link Polynomial}.
   *
   * @throws {Error}
   * if `another` is zero.
   *
   * @version 1.0.0
   * @since 1.0.0
   *
   * @example
   * import { Monomial as M, Polynomial as P } from '@kkitahara/real-algebra'
   *
   * let a = new P(new M(1, 2), new M(1, 2, 5))
   * let b = new P(new M(-1, 2), new M(1, 2, 5))
   * a.idiv(b)
   *
   * b.equals(new P(new M(3, 2, 1), new M(1, 2, 5))) // false
   * a.equals(new P(new M(3, 2, 1), new M(1, 2, 5))) // true
   *
   * @example
   * import { Monomial as M, Polynomial as P } from '@kkitahara/real-algebra'
   *
   * let a = new P(new M(1, 2, 1), new M(1, 2, 5))
   * let b = a
   * a.idiv(a)
   *
   * a === b // true
   * a.equals(new P(new M(1))) // true
   * b.equals(new P(new M(1))) // true
   *
   * a.idiv(null) // Error
   * a.idiv(P.zero()) // Error
   *
   * @example
   * import { Monomial as M, Polynomial as P } from '@kkitahara/real-algebra'
   *
   * let a = new P(new M(1))
   * let b = new P(new M(1, 1, 10), new M(1, 1, 6))
   * a.idiv(b)
   *
   * a.equals(new P(new M(1, 4, 10), new M(-1, 4, 6))) // true
   *
   * @example
   * import { Monomial as M, Polynomial as P } from '@kkitahara/real-algebra'
   *
   * let a = new P(new M(1, 1, 1), new M(1, 1, 2))
   * let b = new P(new M(1, 1, 2))
   * a.idiv(b)
   *
   * a.equals(new P(new M(1, 1, 1), new M(1, 2, 2))) // true
   */
  idiv (another) {
    if (!(another instanceof Polynomial)) {
      throw Error('`another` is not an instance of `Polynomial`.')
    } else if (another.isZero()) {
      throw Error('`another` is zero.')
    }
    const denom = another.copy()
    while (denom.length > 1) {
      const n = denom.length - 1
      let sqFac = denom[n].sqBasis
      const conjugate = new Polynomial(denom[n].copy().ineg())
      for (let i = n - 1; i >= 0; i -= 1) {
        const gcd = bigInt.gcd(sqFac, denom[i].sqBasis)
        if (gcd.notEquals(bigInt.one)) {
          sqFac = gcd
          conjugate.push(denom[i].copy().ineg())
        } else {
          conjugate.push(denom[i].copy())
        }
      }
      this.imul(conjugate)
      denom.imul(conjugate)
    }
    for (let i = this.length - 1; i >= 0; i -= 1) {
      this[i].idiv(denom[0])
    }
    if (denom[0].sqBasis.notEquals(1)) {
      this.sort()
    }
    return this
  }

  /**
   * @desc
   * The Polynomial#ineg method multiplies `this` by `-1` *in place*.
   *
   * @return {Polynomial}
   * `this`.
   *
   * @version 1.0.0
   * @since 1.0.0
   *
   * @example
   * import { Monomial as M, Polynomial as P } from '@kkitahara/real-algebra'
   *
   * let a = new P(new M(1, 2), new M(1, 2, 5))
   * let b = a
   * let c = new P(new M(-1, 2), new M(-1, 2, 5))
   * let d = a.copy()
   * a.ineg()
   *
   * a === b // true
   * a === d // false
   * a.equals(c) // true
   * a.equals(d) // false
   * b.equals(c) // true
   * b.equals(d) // false
   */
  ineg () {
    for (let i = this.length - 1; i >= 0; i -= 1) {
      this[i].ineg()
    }
    return this
  }

  /**
   * @desc
   * The Polynomial#valueOf method converts `this` to a built-in {@link number}.
   *
   * @return {number}
   * the numerical value of `this`.
   *
   * @version 1.0.0
   * @since 1.0.0
   *
   * @example
   * import { Monomial as M, Polynomial as P } from '@kkitahara/real-algebra'
   *
   * let a = new P(new M(1, 2), new M(1, 2, 5))
   * let tau = (1 + Math.sqrt(5)) / 2
   *
   * a.valueOf() // tau
   * 0 + a // tau
   */
  valueOf () {
    return this.reduce((sum, currentValue) => {
      return sum + currentValue.valueOf()
    }, 0)
  }

  /**
   * @desc
   * The Polynomial#toString method converts
   * `this` to a human-readable {@link string}.
   *
   * @param {number} [radix]
   * the base to use for representing numeric values.
   *
   * @return {string}
   * a human-readable {@link string} representation of `this`.
   *
   * @version 1.0.4
   * @since 1.0.0
   *
   * @example
   * import { Monomial as M, Polynomial as P } from '@kkitahara/real-algebra'
   *
   * let a = new P(new M(-1, 2), new M(1, 2, 5))
   *
   * a.toString() // '-1 / 2 + (1 / 2)sqrt(5)'
   * a.toString(2) // '-1 / 10 + (1 / 10)sqrt(101)'
   *
   * new P().toString() // '0'
   */
  toString (radix) {
    if (this.isZero()) {
      return bigInt.zero.toString(radix)
    }
    let s = ''
    for (let i = 0, n = this.length; i < n; i += 1) {
      s += this[i].toString(radix, s.length > 0)
    }
    return s
  }

  /**
   * @desc
   * The Polynomial#toFixed method returns
   * the fixed-point representation of `this`.
   *
   * @param {number} [digits]
   * the number of digits appear after the decimal point.
   *
   * @return {string}
   * the fixed-point representation of `this`.
   *
   * @version 1.0.0
   * @since 1.0.0
   *
   * @example
   * import { Monomial as M, Polynomial as P } from '@kkitahara/real-algebra'
   *
   * let a = new P(new M(1, 2), new M(1, 2, 5))
   *
   * a.toFixed(3) // '1.618'
   */
  toFixed (digits) {
    return this.valueOf().toFixed(digits)
  }

  /**
   * @desc
   * The Polynomial#toJSON method converts
   * `this` to an object serialisable by `JSON.stringify`.
   *
   * @return {object}
   * a serialisable object for `this`.
   *
   * @version 1.0.0
   * @since 1.0.0
   *
   * @example
   * import { Monomial as M, Polynomial as P } from '@kkitahara/real-algebra'
   *
   * let a = new P(new M(-1), new M(1))
   *
   * // toJSON method is called by JSON.stringify
   * let s = JSON.stringify(a)
   *
   * typeof s // 'string'
   */
  toJSON () {
    const radix = 36
    const obj = {}
    obj.reviver = 'Polynomial'
    obj.version = '1.0.0'
    obj.radix = radix
    obj.data = []
    for (let i = 0, n = this.length; i < n; i += 1) {
      obj.data.push([
        this[i].coef.num.toString(radix),
        this[i].coef.denom.toString(radix),
        this[i].sqBasis.toString(radix)])
    }
    return obj
  }

  /**
   * @desc
   * The Polynomial.reviver function converts
   * the data produced by {@link Polynomial#toJSON}
   * into an instance of {@link Polynomial}.
   *
   * @param {object} key
   *
   * @param {object} value
   *
   * @return {Polynomial|object}
   * an instance of {@link Polynomial} or `value`.
   *
   * @throws {Error}
   * if the given object is invalid.
   *
   * @version 1.0.0
   * @since 1.0.0
   *
   * @example
   * import { Monomial as M, Polynomial as P } from '@kkitahara/real-algebra'
   *
   * let a = new P(new M(-1, 2), new M(1, 2, 5))
   * let s = JSON.stringify(a)
   *
   * let b = JSON.parse(s, P.reviver)
   *
   * typeof s // 'string'
   * a.equals(b) // true
   * a === b // false
   *
   * let s2 = s.replace('1.0.0', '0.0.0')
   * JSON.parse(s2, P.reviver) // Error
   */
  static reviver (key, value) {
    if (value !== null && typeof value === 'object' &&
        value.reviver === 'Polynomial') {
      if (value.version === '1.0.0') {
        const radix = value.radix
        const data = value.data
        const arr = []
        for (let i = 0, n = data.length; i < n; i += 1) {
          const numCoef = bigInt(data[i][0], radix)
          const denomCoef = bigInt(data[i][1], radix)
          const sqBasis = bigInt(data[i][2], radix)
          arr.push(new Monomial(numCoef, denomCoef, sqBasis))
        }
        return new Polynomial(...arr)
      } else {
        throw Error('invalid version.')
      }
    } else {
      return value
    }
  }
}

/* @license-end */
