/**
 * @source: https://www.npmjs.com/package/@kkitahara/real-algebra
 * @license magnet:?xt=urn:btih:8e4f440f4c65981c5bf93c76d35135ba5064d8b7&dn=apache-2.0.txt Apache-2.0
 */

/**
 * Copyright 2019 Koichi Kitahara
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

import { Monomial, Polynomial } from './number-field.mjs'
import { RealAlgebra } from './real-algebra.mjs'

/**
 * @desc
 * The ExactRealAlgebra class implements
 * the interfaces of {@link RealAlgebra} by using {@link Polynomial}s
 * as {@link RealAlgebraicElement}s.
 *
 * If a method is called with {@link number}s where {@link Polynomial}s
 * are required, the {@link number}s are casted to {@link Polynomial}s
 * by using {@link ExactRealAlgebra#cast} method.
 *
 * @version 1.0.0
 * @since 1.0.0
 */
export class ExactRealAlgebra extends RealAlgebra {
  /**
   * @desc
   * The ExactRealAlgebra#num method returns
   * an instance of {@link Polynomial} representing
   * (*p* / *q*)sqrt(*b*),
   * where *p* is an integer, *q* is a non-zero integer,
   * and *b* is a positive, square-free integer.
   *
   * `ralg.num(p, q, b)` is an alias for
   * `new Polynomial(new Monomial(p, q, b))`,
   * where `ralg` is an instance of {@link ExactRealAlgebra}.
   *
   * @param {number|bigInt} [p = 0]
   * an integer.
   *
   * @param {number|bigInt} [q = 1]
   * a non-zero integer (can be negative).
   *
   * @param {number|bigInt} [b = 1]
   * a positive, square-free integer.
   *
   * @return {Polynomial}
   * an instance of {@link Polynomial} representing `(p / q)sqrt(b)`.
   *
   * @version 1.0.0
   * @since 1.0.0
   */
  num (p = 0, q = 1, b = 1) {
    return new Polynomial(new Monomial(p, q, b))
  }

  /**
   * @desc
   * The ExactRealAlgebra#cast method just returns
   * `a` if `a` is an instance of {@link Polynomial}
   * and otherwise casts `a` to an instance of {@link Polynomial}
   * by using {@link ExactRealAlgebra#num} method.
   *
   * @param {object} [a = 0]
   * an object.
   *
   * @return {Polynomial}
   * `a` or a new instance of {@link Polynomial}.
   *
   * @version 1.2.1
   * @since 1.0.0
   */
  cast (a = 0) {
    if (!(a instanceof Polynomial)) {
      a = this.num(a)
    }
    return a
  }

  /**
   * @desc
   * The ExactRealAlgebra#copy method returns a copy of `a`.
   *
   * `ralg.copy(a)` is an alias for `a.copy()`,
   * where `ralg` is an instance of {@link ExactRealAlgebra}.
   *
   * @param {Polynomial} a
   * an instance of {@link Polynomial}.
   *
   * @return {Polynomial}
   * a copy of `a`.
   *
   * @version 1.0.0
   * @since 1.0.0
   */
  copy (a) {
    a = this.cast(a)
    return a.copy()
  }

  /**
   * @desc
   * The ExactRealAlgebra#eq method checks if `a` is equal to `b`.
   *
   * `ralg.eq(a, b)` is an alias for `a.equals(b)`,
   * where `ralg` is an instance of {@link ExactRealAlgebra}.
   *
   * @param {Polynomial} a
   * an instance of {@link Polynomial}.
   *
   * @param {Polynomial} b
   * an instance of {@link Polynomial}.
   *
   * @return {boolean}
   * `true` if `a` is equal to `b` and `false` otherwise.
   *
   * @version 1.0.0
   * @since 1.0.0
   */
  eq (a, b) {
    a = this.cast(a)
    b = this.cast(b)
    return a.equals(b)
  }

  /**
   * @desc
   * The ExactRealAlgebra#isZero method checks if `a` is zero.
   *
   * `ralg.isZero(a)` is an alias for `a.isZero()`,
   * where `ralg` is an instance of {@link ExactRealAlgebra}.
   *
   * @param {Polynomial} a
   * an instance of {@link Polynomial}.
   *
   * @return {boolean}
   * `true` if `a` is zero and `false` otherwise.
   *
   * @version 1.0.0
   * @since 1.0.0
   */
  isZero (a) {
    a = this.cast(a)
    return a.isZero()
  }

  /**
   * @desc
   * The ExactRealAlgebra#isPositive method checks
   * if `a` is positive (non-zero).
   *
   * `ralg.isPositive(a)` is an alias for `a.isPositive()`,
   * where `ralg` is an instance of {@link ExactRealAlgebra}.
   *
   * @param {Polynomial} a
   * an instance of {@link Polynomial}.
   *
   * @return {boolean}
   * `true` if `a` is positive (non-zero) and `false` otherwise.
   *
   * @version 1.0.0
   * @since 1.0.0
   */
  isPositive (a) {
    a = this.cast(a)
    return a.isPositive()
  }

  /**
   * @desc
   * The ExactRealAlgebra#isInteger method checks
   * if `a` is an integer.
   *
   * @param {Polynomial} a
   * an instance of {@link Polynomial}.
   *
   * @return {boolean}
   * `true` if `a` is considered to be an integer and `false` otherwise.
   *
   * @version 1.1.0
   * @since 1.1.0
   */
  isInteger (a) {
    a = this.cast(a)
    return a.isInteger()
  }

  /**
   * @desc
   * The ExactRealAlgebra#isFinite method checks if `a` is finite.
   *
   * @param {Polynomial} a
   * an instance of {@link Polynomial}.
   *
   * @return {boolean}
   * `true`.
   *
   * @version 1.0.0
   * @since 1.0.0
   */
  isFinite (a) {
    a = this.cast(a)
    return true
  }

  /**
   * @desc
   * The ExactRealAlgebra#isExact method checks
   * if `this` is an implementation of exact algebra.
   *
   * @return {boolean}
   * `true`.
   *
   * @version 1.0.0
   * @since 1.0.0
   */
  isExact () {
    return true
  }

  /**
   * @desc
   * The ExactRealAlgebra#iadd method adds `b` to `a` *in place*.
   *
   * `ralg.iadd(a, b)` is an alias for `a.iadd(b)`,
   * where `ralg` is an instance of {@link ExactRealAlgebra}.
   *
   * @param {Polynomial} a
   * an instance of {@link Polynomial}.
   *
   * @param {Polynomial} b
   * an instance of {@link Polynomial}.
   *
   * @return {Polynomial}
   * `a`, or a new instance of {@link Polynomial} if `a` is not an instance
   * of {@link Polynomial}.
   *
   * @version 1.0.0
   * @since 1.0.0
   */
  iadd (a, b) {
    a = this.cast(a)
    b = this.cast(b)
    return a.iadd(b)
  }

  /**
   * @desc
   * The ExactRealAlgebra#isub method subtracts `b` from `a` *in place*.
   *
   * `ralg.isub(a, b)` is an alias for `a.isub(b)`,
   * where `ralg` is an instance of {@link ExactRealAlgebra}.
   *
   * @param {Polynomial} a
   * an instance of {@link Polynomial}.
   *
   * @param {Polynomial} b
   * an instance of {@link Polynomial}.
   *
   * @return {Polynomial}
   * `a`, or a new instance of {@link Polynomial} if `a` is not an instance of
   * {@link Polynomial}.
   *
   * @version 1.0.0
   * @since 1.0.0
   */
  isub (a, b) {
    a = this.cast(a)
    b = this.cast(b)
    return a.isub(b)
  }

  /**
   * @desc
   * The ExactRealAlgebra#imul method multiplies `a` by `b` *in place*.
   *
   * `ralg.imul(a, b)` is an alias for `a.imul(b)`,
   * where `ralg` is an instance of {@link ExactRealAlgebra}.
   *
   * @param {Polynomial} a
   * an instance of {@link Polynomial}.
   *
   * @param {Polynomial} b
   * an instance of {@link Polynomial}.
   *
   * @return {Polynomial}
   * `a`, or a new instance of {@link Polynomial} if `a` is not an instance of
   * {@link Polynomial}.
   *
   * @version 1.0.0
   * @since 1.0.0
   */
  imul (a, b) {
    a = this.cast(a)
    b = this.cast(b)
    return a.imul(b)
  }

  /**
   * @desc
   * The ExactRealAlgebra#idiv method divides `a` by `b` *in place*.
   *
   * `ralg.idiv(a, b)` is an alias for `a.idiv(b)`,
   * where `ralg` is an instance of {@link ExactRealAlgebra}.
   *
   * @param {Polynomial} a
   * an instance of {@link Polynomial}.
   *
   * @param {Polynomial} b
   * an instance of {@link Polynomial}.
   *
   * @return {Polynomial}
   * `a`, or a new instance of {@link Polynomial} if `a` is not an instance of
   * {@link Polynomial}.
   *
   * @version 1.0.0
   * @since 1.0.0
   */
  idiv (a, b) {
    a = this.cast(a)
    b = this.cast(b)
    return a.idiv(b)
  }

  /**
   * @desc
   * The ExactRealAlgebra#ineg method multiplies `a` by `-1` *in place*.
   *
   * `ralg.ineg(a)` is an alias for `a.ineg()`,
   * where `ralg` is an instance of {@link ExactRealAlgebra}.
   *
   * @param {Polynomial} a
   * an instance of {@link Polynomial}.
   *
   * @return {Polynomial}
   * `a`, or a new instance of {@link Polynomial} if `a` is not an instance of
   * {@link Polynomial}.
   *
   * @version 1.0.0
   * @since 1.0.0
   */
  ineg (a) {
    a = this.cast(a)
    return a.ineg()
  }

  /**
   * @desc
   * The ExactRealAlgebra#icjg method
   * evaluates the complex conjugate of `a` and stores the result to `a`.
   *
   * @param {Polynomial} a
   * a {@link Polynomial}.
   *
   * @return {Polynomial}
   * `a`, or a new instance of {@link Polynomial} if `a` is not an instance of
   * {@link Polynomial}.
   *
   * @version 1.0.0
   * @since 1.0.0
   */
  icjg (a) {
    a = this.cast(a)
    return a
  }

  /**
   * @desc
   * The ExactRealAlgebra#iabs method multiplies `a` by `-1` *in place*
   * if `a` is negative.
   *
   * @param {Polynomial} a
   * an instance of {@link Polhnomial}.
   *
   * @return {Polynomial}
   * `a`, or a new instance of {@link Polynomial} if `a` is not an instance of
   * {@link Polynomial}.
   *
   * @version 1.0.0
   * @since 1.0.0
   */
  iabs (a) {
    a = this.cast(a)
    if (this.isNegative(a)) {
      return this.ineg(a)
    } else {
      return a
    }
  }

  /**
   * @desc
   * The reviver function for the {@link Polynomial}s.
   *
   * @param {object} key
   *
   * @param {object} value
   *
   * @return {Polynomial|Object}
   * an instance of {@link Polynomial} or `value`.
   *
   * @version 1.0.0
   * @since 1.0.0
   */
  reviver (key, value) {
    return Polynomial.reviver(key, value)
  }
}

/* @license-end */
